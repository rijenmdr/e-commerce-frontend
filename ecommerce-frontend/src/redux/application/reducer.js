import * as actionTypes from './type';

const INITIAL_STATE = {
    loading: false,
    modal: false
}

export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case actionTypes.SET_LOADING:
            return {
                ...state,
                loading: action.payload
            }
        case actionTypes.SET_MODAL:
            return {
                ...state,
                modal: action.payload
            }
        default: 
            return {...state};
    }
}