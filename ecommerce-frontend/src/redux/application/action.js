import * as actionTypes from './type';

export const setLoading = loading => {
    return {
        type: actionTypes.SET_LOADING,
        payload: loading
    }
}

export const setModal = modal => {
    return {
        type: actionTypes.SET_MODAL,
        payload: modal
    }
}