import * as actionTypes from './type';

const INITIAL_STATE = {
    currentUser: null,
    cart: [],
    loading: false,
    error: false
}

export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case actionTypes.INITIATE_USER:
            return {
                ...state,
                loading: true,
                error: false,
            }
        case actionTypes.LOGIN_SUCCESS:
            return {
                // ...state,
                currentUser: action.payload,
                cart: action.payload?.cart,
                loading: false,
                error: false
            }
        case actionTypes.USER_ERROR:
            return {
                ...state,
                loading: false,
                error: true
            }
        case actionTypes.LOGOUT_SUCCESS:
            return {
                ...state,
                currentUser: null,
                cart: []
            }
        case actionTypes.UPDATE_CART_SUCCESS:
            return {
                ...state,
                cart: action.payload
            }
        default: 
            return {...state};
    }
}