import Cookies from 'js-cookie';
import * as actionTypes from './type';

const logoutSuccess = () => {
    return {
        type: actionTypes.LOGOUT_SUCCESS
    }
}

const updateCartSuccess = (cart) => {
    return {
        type: actionTypes.UPDATE_CART_SUCCESS,
        payload: cart
    }
}

export const initialUser = () => {
    return {
        type: actionTypes.INITIATE_USER
    }
}

export const userError = () => {
    return {
        type: actionTypes.USER_ERROR
    }
}

export const loginSuccess = currentUser => {
    // localStorage.setItem("name", currentUser?.name);
    // localStorage.setItem("email", currentUser?.email);
    // currentUser?.mobile && localStorage.setItem("mobile",currentUser?.mobile)
    // currentUser?.dob && localStorage.setItem("dob",currentUser?.dob)
    // currentUser?.gender && localStorage.setItem("gender",currentUser?.gender)
    // currentUser?.country && localStorage.setItem("country",currentUser?.country)
    // currentUser?.city && localStorage.setItem("city",currentUser?.city)
    // currentUser?.street && localStorage.setItem("street",currentUser?.street)
    // localStorage.setItem("cart", JSON.stringify(currentUser?.cart))
    return {
        type: actionTypes.LOGIN_SUCCESS,
        payload: currentUser
    }
}

export const logoutUser = () => dispatch => {
    try {
        //console.log('remove');
        Cookies.remove('token');
        localStorage.clear();
        dispatch(logoutSuccess());
      } catch (error) {
        console.log('>>>>: src/helpers/Utils.js : setCurrentUser -> error', error);
      }
}

export const updateCart = (cart) => dispatch => {
    localStorage.setItem("cart", JSON.stringify(cart));
    dispatch(updateCartSuccess(cart));
}