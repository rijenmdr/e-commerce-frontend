import { combineReducers } from "redux";

import applicationReducer from './application/reducer';
import userReducer from './user/reducer';

const reducers = combineReducers({
    application: applicationReducer,
    user: userReducer
});

export default reducers;