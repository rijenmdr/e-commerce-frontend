export const navItems = [
    {
        id: "61de651d3d339dbb57bad351",
        category_name: "Bakery",
        sub_categories: [
            {
                name: "Breads",
                link: "/breads"
            },
            {
                name: "Biscuits",
                link: "/biscuits"
            }
        ]
    },
    {
        id: "61c5e2df649e325820ffaf19",
        category_name: "Food",
        sub_categories: [
            {
                name: "Apple",
                link: "/apple"
            },
            {
                name: "Cheetos",
                link: "/cheetos"
            },
            {
                name: "Rice",
                link: "/rice"
            },
            {
                name: "Carrot",
                link: "/papaya"
            },
            {
                name: "Raddish",
                link: "/papaya"
            }
        ]
    },
    {
        id: "61de64e63d339dbb57bad34b",
        category_name: "Meat and fish",
        sub_categories: [
            {
                name: "Chicken",
                link: "/chicken"
            },
            {
                name: "Buff",
                link: "/buff"
            },
            {
                name: "Mutton",
                link: "/mutton"
            },
            {
                name: "Prawn Fish",
                link: "/prawn-fish"
            }
        ]
    },
    {
        id: "61de63ee3d339dbb57bad344",
        category_name: "Drinks",
        sub_categories: [
            {
                name: "Coca Cola",
                link: "/coca-cola"
            },
            {
                name: "Apple Juice",
                link: "/apple-juice"
            },
            {
                name: "Tuborg",
                link: "/tuborg"
            },
            {
                name: "Nepal Ice",
                link: "/nepal-ice"
            }
        ]
    },
    {
        id: "61de64f83d339dbb57bad34e",
        category_name: "Kitchen",
        sub_categories: [
            {
                name: "Knife",
                link: "/knife"
            },
            {
                name: "Spoon",
                link: "/spoon"
            },
            {
                name: "Plate",
                link: "/plate"
            },
            {
                name: "Pan",
                link: "/pan"
            }
        ]
    },
    {
        id: "61c5e31c649e325820ffaf1f",
        category_name: "Technology",
        sub_categories: [
            {
                name: "Smart Watch",
                link: "/protein"
            },
            {
                name: "Mobile",
                link: "/cereal"
            },
            {
                name: "Laptop",
                link: "/protein-bar"
            },
        ]
    },
    {
        id: "61de63c53d339dbb57bad33e",
        category_name: "Baby",
        sub_categories: [
            {
                name: "Diaper",
                link: "/diaper"
            },
            {
                name: "Baby Powder",
                link: "/baby-powder"
            },
            {
                name: "Baby Oil",
                link: "/baby-oil"
            },
        ]
    },
    {
        id: "61de63cf3d339dbb57bad341",
        category_name: "Pharmacy",
        sub_categories: [
            {
                name: "Vitamins",
                link: "/vitamins"
            },
            {
                name: "Honey",
                link: "/honey"
            },
            {
                name: "Medicine",
                link: "/medicine"
            },
        ]
    }
]