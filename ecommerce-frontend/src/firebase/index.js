import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore';
import { getAuth } from 'firebase/auth';

const firebaseConfig = {
  apiKey: "AIzaSyBKE_M7IH9pwwVB4ZitjfySd1oxhs2pPvw",
  authDomain: "freshnesecom-4c239.firebaseapp.com",
  projectId: "freshnesecom-4c239",
  storageBucket: "freshnesecom-4c239.appspot.com",
  messagingSenderId: "386888319730",
  appId: "1:386888319730:web:d92fcb4a77ae9c3b7b466b",
  measurementId: "G-JXYLNLGECE"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const db = getFirestore(app);
export const auth = getAuth();

// const analytics = getAnalytics(app);

