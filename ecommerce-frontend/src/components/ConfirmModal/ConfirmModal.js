import React from 'react'
import { Button, Modal, ModalBody, ModalFooter, ModalTitle } from 'react-bootstrap'

const ConfirmModal = ({ modal, closeModal, handleSubmit }) => {
    return (
        <Modal
            centered
            animation
            contentClassName='confirm-content'
            show={modal}
            onHide={closeModal}
        >
            <ModalTitle
                className='p-4'
            >
                <h4>Are you sure ?</h4>
            </ModalTitle>
            <ModalBody>
                <Button
                    onClick={handleSubmit}
                >
                    Confirm
                </Button>
                <Button
                    variant="secondary"
                    onClick={closeModal}
                >
                    Cancel
                </Button>
            </ModalBody>
        </Modal>
    )
}

export default ConfirmModal