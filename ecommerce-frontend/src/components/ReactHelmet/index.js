import React from 'react'
import { Helmet } from 'react-helmet'

const ReactHelmet = ({title}) => {
    return (
        <Helmet>
            <title>{`${title} | Freshnesecom`}</title>
            <meta name="description" content="App Description" />
            <meta name="theme-color" content="#008f68" />
        </Helmet>
    )
}

export default ReactHelmet
