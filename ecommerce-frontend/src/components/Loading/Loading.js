import React from 'react';
import { Image } from 'react-bootstrap';
import loading from '../../assets/images/components/loading.svg';

const Loading = () => {
  return (
    <div className='loading'>
        <Image
            src={loading}
            alt="loading"
        />
    </div>
  )
}

export default Loading