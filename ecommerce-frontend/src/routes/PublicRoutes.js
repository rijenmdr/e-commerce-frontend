import React from 'react';
import { Navigate } from 'react-router-dom';
import Cookies from 'js-cookie';

const PublicRoutes = ({ component: Component, ...rest }) => {
    const token = Cookies.get('token')
    if (token) {
        return (
            <Navigate to="/" replace={true} />
        )
    } else {
        return (
            <Component {...rest} />
        )
    }
}

export default PublicRoutes
