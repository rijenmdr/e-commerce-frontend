import React from 'react';
import { Navigate } from 'react-router-dom';
import Cookies from 'js-cookie';

const PrivateRoutes = ({ component: Component, ...rest }) => {
    const token = Cookies.get('token');
    if (!token) {
        return (
            <Navigate to="/" replace={true} />
        )
    } else {
        return (
            <Component {...rest} />
        )
    }
}

export default PrivateRoutes
