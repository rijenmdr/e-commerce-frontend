import React, { Suspense } from 'react';
import { Routes, Route } from 'react-router-dom';
import Loading from '../views/Landing/components/Loading';
import PublicRoutes from './PublicRoutes';

const LandingLayout = React.lazy(() =>
    import('../views/Landing')
);

const AuthenticationLayout = React.lazy(() =>
    import('../views/Authetication')
);

const AppRoutes = () => {
    return (
        <Suspense fallback={<Loading />}>
            <Routes>
                <Route path="/user/*" element={<PublicRoutes component={AuthenticationLayout} />} />
                <Route path="/*" element={<LandingLayout />} />
            </Routes>
        </Suspense>
    )
}

export default AppRoutes
