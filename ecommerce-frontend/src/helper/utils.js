import Cookies from "js-cookie";
import { initialUser } from "../redux/user/actions";
import axios from "./axios";

export const getCurrentUser = async(dispatch) => {
    dispatch(initialUser());
    const user = await axios.get('/user/current-user', {
        headers: {
            Authorization: 'Bearer ' + Cookies.get('token'),
        }
    });
    return user;
}

export const addProductToFavourite = async(productId) => {
    const favourite = await axios.post('/product/add-to-favourite', {
        productId
    } , {
        headers: {
            Authorization: 'Bearer ' + Cookies.get('token'),
        }
    });
    return favourite;
}