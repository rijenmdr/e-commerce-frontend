import axios from 'axios';
import store from '../redux/store';
import { logoutUser } from '../redux/user/actions';

const API_URL = process.env.REACT_APP_API_URL

console.log(process.env.REACT_APP_API_URL)

const axiosInstances = axios.create({
    baseURL: API_URL,
    headers: {
        Accept: 'application/json'
    }
});

axiosInstances.interceptors.response.use(
    (response) => response,
    async (error) => {
        if( error.response.status &&
            (error.response.status === 401 || error.response.status === 403)
          ) {
              window.history.go('/')
              await store.dispatch(logoutUser());
          }
        throw error;
    }
)

export default axiosInstances;