import { toast } from 'react-toastify';

export const errorResponse = (error) => {
    const errorResponse = error?.response
    if (errorResponse) {
        const res = errorResponse?.data?.message;
        toast.error(res);
    } else {
        toast.error("Error while making request");
    }
};

