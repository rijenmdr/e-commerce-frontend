import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { QueryClientProvider, QueryClient } from 'react-query';

import store from './redux/store';

import App from './App';

//css
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';  
import { Provider } from 'react-redux';

const queryClient = new QueryClient();

const app = (
    <Provider store={store}>
        <QueryClientProvider client={queryClient}>
            <BrowserRouter>
                <App/>
            </BrowserRouter>
        </QueryClientProvider>
    </Provider>
   
)

ReactDOM.render(app,document.getElementById('root'))