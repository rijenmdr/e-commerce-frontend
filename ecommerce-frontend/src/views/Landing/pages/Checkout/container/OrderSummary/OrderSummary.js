import React, { useEffect, useState } from 'react';
import { Button, Col, Row } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import TextInput from '../../../../components/TextInput/TextInput';
import OrderList from '../../components/OrderList/OrderList';

const OrderSummary = ({ cart, amount, setAmount, shippingCost, setShippingCost }) => {
    const [promo, setPromo] = useState('');
    const [delivery, setDelivery] = useState('');

    console.log(cart)

    const calculateSummary = () => {
        let productAmount = 0;
        let shipCost = cart[0]?.shippingCost;
        let delivery = cart[0]?.numberForDelivery;

        cart && cart.map((cartProduct) => {
            productAmount = productAmount + cartProduct?.actualPrice * cartProduct?.numberOfItem;

            if (shipCost < cartProduct?.shippingCost) {
                shipCost = cartProduct?.shippingCost;
            }

            if(!cartProduct?.deliveryIn){
                delivery = delivery < cartProduct?.numberForDelivery ? cartProduct?.numberForDelivery : delivery
            } else {
                delivery = cartProduct?.deliveryIn?.numberForDelivery;
                return;
            }

        });
        setAmount(productAmount);
        setShippingCost(shipCost);
        setDelivery(delivery)
    }

    useEffect(() => {
        if (cart && cart.length !== 0) {
            calculateSummary();
        }
    }, [cart]);
    return (
        <div className='order-summary'>
            <h2>Order Summary</h2>
            <h5 className='text-muted'>Price can change depending on shipping method and taxes of your state.</h5>
            <div className='order-list'>
                {
                    cart &&
                    cart.map(item => (
                        <OrderList
                            key={item?.id}
                            checkoutList={item}
                        />
                    ))
                }
            </div>
            <div className='d-flex justify-content-between'>
                <h4>Subtotal</h4>
                <h4>Nrp. {amount}</h4>
            </div>
            <div className='d-flex justify-content-between'>
                <h4>Shipping</h4>
                <h4>Nrp. {shippingCost}</h4>
            </div>
            <Row className='promo-code align-items-center mt-4'>
                <Col xl="9">
                    <TextInput
                        label=''
                        type='text'
                        name='promo'
                        value={promo}
                        handleChange={(e) => setPromo(e.target.value)}
                        placeholder='Apply Promo Code'                
                    />
                </Col>
                <Col xl="3">
                    <Button
                        size='sm'
                        type="primary"
                    >
                        Apply
                    </Button>
                </Col>
            </Row>
            <div className='d-flex justify-content-between align-items-center mt-4'>
                <div>
                    <h5 className='mb-0'>Total Order</h5>
                    <h5 className='text-muted'>Delivery within {delivery} day(s)</h5>
                </div>
                <h2 className='mb-0 text-secondary'>NRp. {amount + shippingCost}</h2>
            </div>
        </div>
    )
};

export default OrderSummary;
