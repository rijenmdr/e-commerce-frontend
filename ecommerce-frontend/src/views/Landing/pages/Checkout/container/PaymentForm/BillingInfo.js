import React from 'react';
import { Formik } from 'formik';
import { Button, Col, Form, Row } from 'react-bootstrap';
import TextInput from '../../../../components/TextInput/TextInput';
import * as Yup from 'yup';
import { mobileNoValidation } from '../../../../../../helper/validation';

const billingInfoValidation = Yup.object().shape({
    firstName: Yup.string().required("First Name is required"),
    lastName: Yup.string().required("Last Name is required"),
    email: Yup.string()
    .required("Email is required")
    .email("*Email Address is invalid"),
    mobile: Yup.string()
    .required("*Mobile Number is required")
    .matches(mobileNoValidation, "Invalid Mobile Number"),
    street: Yup.string().required("Street is required"),
    city: Yup.string().required("City is required"),
    country: Yup.string().required("Country is required"),
    zip: Yup.string().required("Zip/Postal Code is required")
});

const BillingInfo = ({ data, setData, handleNextStep }) => {
  return (
      <div className='billing-info'>
          <div className='d-flex justify-content-between align-items-center'>
            <div>
                <h2 className='mb-0'>Billing Info</h2>
                <h5 className='text-muted'>Please enter your billing info</h5>
            </div>
                <h5 className='text-muted'>Step 1 of 3</h5>
          </div>
          <Formik
            initialValues={data}
            validationSchema={billingInfoValidation}
            onSubmit={(values, { setSubmitting, resetForm }) => {
                console.log(values)
                handleNextStep(values)
            }}
        >
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
            }
            ) => (
                <Form onSubmit={handleSubmit}>
                    <Row className='mt-4'>
                        <Col xl="6">
                            <TextInput
                                name="firstName"
                                label="First Name"
                                type="text"
                                value={values.firstName}
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                                placeholder={"Enter your First Name"}
                                // disabled={isLoading}
                                touched={touched.firstName}
                                errors={errors.firstName}
                            />
                        </Col>

                        <Col xl="6">
                            <TextInput
                                name="lastName"
                                label="Last Name"
                                type="text"
                                value={values.lastName}
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                                placeholder={"Enter your Last Name"}
                                // disabled={isLoading}
                                touched={touched.lastName}
                                errors={errors.lastName}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col lg="6">
                            <TextInput
                                name="email"
                                label="Email"
                                type="text"
                                value={values.email}
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                                placeholder={"Enter your Email"}
                                // disabled={loading}
                                touched={touched.email}
                                errors={errors.email}
                            />
                        </Col>
                        <Col xl="6">
                            <TextInput
                                name="mobile"
                                label="Mobile Number"
                                type="text"
                                value={values.mobile}
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                                placeholder={"Enter your Mobile Number"}
                                touched={touched.mobile}
                                errors={errors.mobile}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col xl="6">
                            <TextInput
                                name="street"
                                label="Street"
                                type="text"
                                value={values.street}
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                                placeholder={"Enter your Street"}
                                // disabled={isLoading}
                                touched={touched.street}
                                errors={errors.street}
                            />
                        </Col>

                        <Col xl="6">
                            <TextInput
                                name="city"
                                label="City"
                                type="text"
                                value={values.city}
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                                placeholder={"Enter your City"}
                                // disabled={isLoading}
                                touched={touched.city}
                                errors={errors.city}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col xl="6">

                            <TextInput
                                name="country"
                                label="Country"
                                type="text"
                                value={values.country}
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                                placeholder={"Enter your Country"}
                                // disabled={isLoading}
                                touched={touched.country}
                                errors={errors.country}
                            />
                        </Col>
                        <Col xl="6">
                            <TextInput
                                name="zip"
                                label="Zip/ Postal Code"
                                type="text"
                                value={values.zip}
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                                placeholder={"Enter your Postal Code/Zip"}
                                // disabled={isLoading}
                                touched={touched.zip}
                                errors={errors.zip}
                            />
                        </Col>
                    </Row>
                    <div className='d-flex justify-content-end mt-4'>
                        <Button
                            type="submit"
                            variant='primary'
                        >
                            Continue
                        </Button>
                </div>
                </Form>
            )}
        </Formik>
      </div>
  );
};

export default BillingInfo;
