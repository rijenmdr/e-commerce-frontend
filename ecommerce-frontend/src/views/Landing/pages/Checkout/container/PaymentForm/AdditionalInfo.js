import React from 'react';
import { Formik } from 'formik';
import { Button, Col, Form, Row } from 'react-bootstrap';
import TextAreaInput from '../../../../components/TextAreaInput/TextAreaInput';
import * as Yup from 'yup';

const additionalInfoValidation = Yup.object().shape({
    orderNote: Yup.string().required("Order Note is required"),
});

const AdditionalInformation = ({ data, handleNextStep, handlePrevStep }) => {
  return (
      <div className='billing-info'>
          <div className='d-flex justify-content-between align-items-center'>
            <div>
                <h2 className='mb-0'>Additional informations</h2>
                <h5 className='text-muted'>Need something else? We will make it for you!</h5>
            </div>
                <h5 className='text-muted'>Step 2 of 3</h5>
          </div>
          <Formik
            initialValues={data}
            validationSchema={additionalInfoValidation}
            onSubmit={(values, { setSubmitting, resetForm }) => {
                handleNextStep(values)
            }}
        >
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
            }
            ) => (
                <Form onSubmit={handleSubmit}>
                    <Row className='mt-4'>
                        <Col xl="12">
                            <TextAreaInput
                                name="orderNote"
                                label="Order Note"
                                value={values.orderNote}
                                handleChange={handleChange}
                                row={5}
                                handleBlur={handleBlur}
                                placeholder={"Need a specific delivery day? Sending a gitf? Let’s say ..."}
                                // disabled={isLoading}
                                touched={touched.orderNote}
                                errors={errors.orderNote}
                            />
                        </Col>

                       
                    </Row>
                    <div className='d-flex justify-content-between mt-4'>
                        <Button
                            type="button"
                            variant='secondary'
                            onClick={() => handlePrevStep(values)}
                        >
                            Back
                        </Button>
                        <Button
                            type="submit"
                            variant='primary'
                        >
                            Continue
                        </Button>
                </div>
                </Form>
            )}
        </Formik>
      </div>
  );
};

export default AdditionalInformation;
