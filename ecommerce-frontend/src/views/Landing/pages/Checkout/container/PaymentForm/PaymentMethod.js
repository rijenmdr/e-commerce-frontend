import React from 'react';
import { Button, Card, Col, Row, Spinner } from 'react-bootstrap';
import SelectButton from '../../components/SelectButton/SelectButton';

const PaymentMethod = ({ paymentMethod, setPaymentMethod, handlePrevStep, handlePayment, loading }) => {

    const changePaymentMethod = (method) => {
        setPaymentMethod(method);
    }

    return (
        <div className='payment-method'>
            <div className='d-flex justify-content-between align-items-center'>
                <div>
                    <h2 className='mb-0'>Payment method</h2>
                    <h5 className='text-muted'>Please enter your payment method</h5>
                </div>
                <h5 className='text-muted'>Step 3 of 3</h5>
            </div>
            <Row>
                <Col className='mt-2 mb-2' xl="12" >
                    <Card
                        onClick={() => changePaymentMethod('cod')}
                        className='p-4'
                    >
                        <div className='d-flex flex-row align-items-center cursor-pointer'>
                            <SelectButton
                                active={paymentMethod === "cod"}
                            />
                            <h4 className='mb-0 ml-md'>Cash on Delivery</h4>
                        </div>
                        {
                            paymentMethod === "cod" &&
                            <div className='p-4'>
                                <h5>You can pay in cash to our courier when you receive the goods at your doorstep.</h5>
                            </div>
                        }
                    </Card>
                </Col>
                <Col xl="12">
                    <Card
                        onClick={() => changePaymentMethod('khalti')}
                        className='p-4 cursor-pointer'
                    >
                        <div className='d-flex flex-row align-items-center'>
                            <SelectButton
                                active={paymentMethod === "khalti"}
                            />
                            <h4 className='mb-0 ml-md'>Khalti</h4>
                        </div>
                        {
                            paymentMethod === "khalti" &&
                            <div className='p-4'>
                                <h5>You will be redirected to your Khalti account to complete your payment:</h5>
                                <h5><li>Login to your Khalti account using your Khalti ID and your Password</li></h5>
                                <h5><li>Ensure your Khalti account is active and has sufficient balance</li></h5>
                                <h5><li>Enter OTP (one time password) sent to your registered mobile number</li></h5>
                                <h5> ***Login with your Khalti mobile and PASSWORD (not MPin)***</h5>
                            </div>
                        }
                    </Card>
                </Col>
            </Row>
            <div className='d-flex justify-content-between mt-4'>
                <Button
                    type="button"
                    variant='secondary'
                    disabled={loading}
                    onClick={handlePrevStep}
                >
                    Back
                </Button>
                <Button
                    type="button"
                    variant='primary'
                    disabled={loading}
                    onClick={handlePayment}
                >
                    {
                        loading ?
                            <Spinner
                                variant='primary'
                                animation='border'
                                size='sm'
                            /> :
                        'Complete Order'
                    }
                </Button>
            </div>
        </div>
    );
};

export default PaymentMethod;
