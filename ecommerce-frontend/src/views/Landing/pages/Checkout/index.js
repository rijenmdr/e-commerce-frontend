import Cookies from 'js-cookie';
import React, { useEffect, useState } from 'react';
import { Col, Row } from 'react-bootstrap';
import { useMutation } from 'react-query';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import ReactHelmet from '../../../../components/ReactHelmet';
import axiosInstances from '../../../../helper/axios';
import { errorResponse } from '../../../../helper/errorResponse';
import { updateCart } from '../../../../redux/actions';
import Breadcrumbs from '../../container/Breadcrumb';

import './Checkout.scss';
import OrderSummary from './container/OrderSummary/OrderSummary';
import AdditionalInformation from './container/PaymentForm/AdditionalInfo';
import BillingInfo from './container/PaymentForm/BillingInfo';
import PaymentMethod from './container/PaymentForm/PaymentMethod';

import KhaltiCheckout from "khalti-checkout-web";

const Checkout = () => {
  const [checkoutProduct, setCheckoutProduct] = useState([]);
  const [stage, setStage] = useState(1);
  const [loading, setLoading] = useState(false);
  const [amount, setAmount] = useState(0);
  const [shippingCost, setShippingCost] = useState(0);

  const user = useSelector(state => state.user.currentUser);

  const [data, setData] = useState({
    firstName: user?.name.split(" ")[0],
    lastName: user?.name.split(" ")[1],
    email: user?.email,
    street: user?.address?.street,
    city: user?.address?.city,
    country: user?.address?.country,
    mobile: user?.mobileNo,
    zip: "",
    orderNote: ""
  });

  const [paymentMethod, setPaymentMethod] = useState('cod');

  const navigate = useNavigate();
  const { state } = useLocation();
  console.log(state);

  let config = {
    // replace this key with yours
    "publicKey": "test_public_key_dc74e0fd57cb46cd93832aee0a390234",
    "productIdentity": "1234567890",
    "productName": "Drogon",
    "productUrl": "http://gameofthrones.com/buy/Dragons",
    "eventHandler": {
        onSuccess (payload) {
            // hit merchant api for initiating verfication
            console.log(payload);
        },
        // onError handler is optional
        onError (error) {
            // handle errors
            console.log(error);
        },
        onClose () {
            console.log('widget is closing');
        }
    },
    "paymentPreference": ["KHALTI", "EBANKING","MOBILE_BANKING", "CONNECT_IPS", "SCT"],
};

  const cart = useSelector(state => state.user.cart);

  const dispatch = useDispatch();

  const mutation = useMutation(async () => {
    setLoading(true);

    const postData = {
      products: checkoutProduct,
      name: `${data?.firstName} ${data?.lastName}`,
      email: data?.email,
      mobile: data?.mobile,
      street: data?.street,
      city: data?.street,
      country: data?.country,
      zip: data?.zip,
      additionalInformation: data?.orderNote
    }

    return await axiosInstances.post('/product/place-order', postData, {
      headers: {
        'Authorization': 'Bearer ' + Cookies.get('token')
      }
    })
  }, {
    onSuccess: (res) => {
      toast.success("Product ordered successfully");
      !state && dispatch(updateCart([]));
      navigate('/checkout-success',{ state: {"message": "success"} })
      setLoading(false);
    },
    onError: (res) => {
      errorResponse(res)
      setLoading(false)
    }
  });

  useEffect(() => {
    if (state) {
      setCheckoutProduct([...checkoutProduct, { id: state?._id, ...state }]);
    } else if (cart.length !== 0) {
      setCheckoutProduct(cart);
    } else {
      navigate('/');
    }
  }, []);

  useEffect(() => {
    window.scrollTo(0, 200);
  }, [stage]);

  const handleNextStep = (values) => {
    setData((prev) => ({ ...prev, ...values }));
    setStage(prevVal => prevVal + 1);
  }

  const handlePrevStep = (values) => {
    values && setData((prev) => ({ ...prev, ...values }));
    setStage(prevVal => prevVal - 1);
  }

  const handlePayment = () => {
    if (paymentMethod === "cod") {
      setLoading(true);
      mutation.mutate();
    } else {
      let checkout = new KhaltiCheckout(config);
      checkout.show({amount: (amount + shippingCost) * 100});
    }
  }

  console.log("data", data)

  return (
    <div className='checkout'>
      <ReactHelmet
        title="Checkout"
      />
      <Breadcrumbs />
      <Row className='section-padding w-100'>
        <Col className='pr-xl mb-4' xl="7">
          {
            stage === 1 &&
            <BillingInfo
              data={data}
              handleNextStep={handleNextStep}
            />
          }
          {
            stage === 2 &&
            <AdditionalInformation
              data={data}
              handleNextStep={handleNextStep}
              handlePrevStep={handlePrevStep}
            />
          }
          {
            stage === 3 &&
            <PaymentMethod
              paymentMethod={paymentMethod}
              setPaymentMethod={setPaymentMethod}
              handlePrevStep={handlePrevStep}
              handlePayment={handlePayment}
              loading={loading}
              setLoading={setLoading}
            />
          }
        </Col>
        <Col xl="5">
          <OrderSummary
            cart={checkoutProduct}
            amount={amount}
            setAmount={setAmount}
            shippingCost={shippingCost}
            setShippingCost={setShippingCost}
          />
        </Col>
      </Row>
    </div>
  );
};

export default Checkout;
