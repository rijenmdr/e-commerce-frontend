import React from 'react';
import { Card, Col, Dropdown, Image, Row } from 'react-bootstrap';
import StarRatings from 'react-star-ratings';
import ProductExtraInfo from '../../../ProductDetail/components/ProductExtraInfo/ProductExtraInfo';
import SelectNoOfItem from '../../../ProductDetail/components/SelectNoOfItem/SelectNoOfItem';

const OrderList = ({ checkoutList }) => {
    return (
        <Row className='py-4'>
            <Col xl="4" lg="4" md="4" sm="4">
                <Card
                >
                    <Image
                        style={{
                            backgroundSize: "cover",
                            maxHeight: "150px"
                        }}
                        src={checkoutList?.previewImg}
                    />
                </Card>
            </Col>
            <Col xl="8" lg="8" md="8" sm="8">
                <h3>{checkoutList?.name}</h3>
                <div className='d-flex flex-column mb-1'>
                    <ProductExtraInfo
                        title="On Stock"
                        value={checkoutList?.onStock}
                        margin="mb-0"
                    />
                    <ProductExtraInfo
                        title="Delivery In"
                        value={
                            checkoutList?.deliveryIn 
                            ? `${checkoutList?.deliveryIn?.numberForDelivery} ${checkoutList?.deliveryIn?.timeForDelivery}`
                            :`${checkoutList?.numberForDelivery} ${checkoutList?.timeForDelivery}`
                        }
                        margin="mb-0"
                    />
                    <ProductExtraInfo
                        title="Delivery Cost"
                        value={checkoutList?.shippingCost !== 0 ? `Nrp. ${checkoutList?.shippingCost}` : 'Free'}
                        margin="mb-0"
                    />
                </div>
                <StarRatings
                    rating={Number(checkoutList?.averageRating)}
                    starRatedColor="#FDBC15"
                    starDimension='20'
                    starSpacing='5'
                    numberOfStars={5}
                />
                <div className='d-flex justify-content-between align-items-center mt-2'>
                    <div className='product-prices'>
                        <h3 className='mb-0 text-secondary'>NRp. {checkoutList?.actualPrice}</h3>
                    </div>
                    <div style={{
                        border: "1px solid #D1D1D1",
                        borderRadius: "12px",
                        padding: "12px 16px"
                    }}>
                       <h5 className='mb-0'>{checkoutList?.numberOfItem} <span>{checkoutList?.soldIn}</span></h5>
                    </div>
                </div>
            </Col>
            <Dropdown.Divider className='mt-4' />
        </Row>
    );
};

export default OrderList;
