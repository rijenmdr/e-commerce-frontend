import React from 'react';

const SelectButton = ({ active }) => {
  return (
        <div className={`outer-circle ${active ? 'active' :''}`}>
            <div className='inner-circle'>

            </div>
        </div>
    );
};

export default SelectButton;
