import React, { useEffect } from 'react';
import { Image } from 'react-bootstrap';
import { Navigate, useLocation, useNavigate } from 'react-router-dom';

import Success from '../../../../assets/images/components/purchaseSuccess.svg';
import ReactHelmet from '../../../../components/ReactHelmet';

const CheckoutSuccess = () => {
    const navigation = useNavigate();

    const { state } = useLocation();

    useEffect(() => {
        // if(!state){
        //     navigation(-1)
        // }

        window.scrollTo(0, 200);
    }, []);

    if(!state) {
        return <Navigate to ="/" />
    } else {
        return (
            <div className='error-page d-flex justify-content-center align-items-center flex-column section-margin'>
                <ReactHelmet
                    title="Checkout Success"
                />
                <h2 className='text-center'>
                    Thankyou for shopping with us.
                </h2>
                <h3 className='mt-2 text-center'>
                    Your product will be delivered to you as soon as possible.
                </h3>
                <Image
                    src={Success}
                    width={400}
                />
                <h4 className='mt-2 text-center'>
                    In a mean time you can discover new <span onClick={() => navigation('/products?page=1')} className='text-secondary cursor-pointer'>products</span>.
                </h4>
            </div>
        );
    }
};

export default CheckoutSuccess;
