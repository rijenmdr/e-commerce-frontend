import React, { useState } from 'react'
import { useQuery } from 'react-query'
import { useLocation } from 'react-router-dom'
import ReactHelmet from '../../../../components/ReactHelmet'
import axios from '../../../../helper/axios'
import Breadcrumbs from '../../container/Breadcrumb'
import BlogContent from './container/BlogContent/BlogContent'
import BlogHeader from './container/BlogHeader/BlogHeader'

const fetchBlogByPage = async (parameter) => {
    const [key, currentPage, category, archive] = parameter.queryKey;
    console.log(key)
    const data = await axios.get(`/blog/get-blogs?page=${currentPage}${category ? `&categoryId=${category}` : ''}${archive ? `&archive=${archive}` : ''}`)
    return data;
}

const fetchSecondaryFeaturedBlogs = async () => {
    const data = await axios.get(`/blog/get-secondary-feature`)
    return data;
}

const Blogs = () => {
    const { search } = useLocation();
    const allQueryParam = new URLSearchParams(search)
    const archive = allQueryParam.get('archive');
    const category = allQueryParam.get('category');

    const [featuredBlogs, setFeaturedBlogs] = useState([]);

    const [blogCount, setBlogCount] = useState(0);
    const [blogs, setBlogs] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);

    const { status } = useQuery(['blogs', currentPage, category, archive], fetchBlogByPage, {
        keepPreviousData: true,
        enabled: !!currentPage || !!archive || !!category,
        retry: false,
        onSuccess: (res) => {
            const result = {
                status: res.status + "-" + res.statusText,
                headers: res.headers,
                data: res.data,
            };
            setBlogs(result.data.blogs);
            setBlogCount(result.data.blogCount)
        }
    });

    useQuery('featuredBlogs', fetchSecondaryFeaturedBlogs, {
        keepPreviousData: true,
        retry: false,
        onSuccess: (res) => {
            const result = {
                status: res.status + "-" + res.statusText,
                headers: res.headers,
                data: res.data,
            };
            setFeaturedBlogs(result.data.featuredBlogs);
        }
    });

    return (
        <div className='blogs-search'>
            <ReactHelmet
                title="Blogs"
            />
            <Breadcrumbs />
            <div className='blog-search-header section-margin'>
                <BlogHeader
                    blogs={blogs}
                    featuredBlogs={featuredBlogs}
                    search={search}
                    archive={archive}
                    category={category}
                />
            </div>
            <div className='blog-search-content section-margin'>
                <BlogContent
                    blogs={blogs}
                    setBlogs={setBlogs}
                    blogCount={blogCount}
                    setBlogCount={setBlogCount}
                    currentPage={currentPage}
                    setCurrentPage={setCurrentPage}
                    status={status}
                />
            </div>
        </div>
    )
}

export default Blogs
