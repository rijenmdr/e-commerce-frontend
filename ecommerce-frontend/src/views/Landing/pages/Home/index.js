import React, { useEffect, useState } from 'react';

//containers
import CategoryMenu from './container/CategoryMenu/CategoryMenu';
import BestSellingProducts from './container/BestSellingProducts/BestSellingProducts';
import BestFromFarmers from './container/BestFromFarmers/BestFromFarmers';
import PopularProducts from './container/PopularProducts/PopularProducts';
import CustomerTestimony from './container/CustomerTestimony/CustomerTestimony';
import OurBlogs from './container/OurBlogs/OurBlogs';

import axios from '../../../../helper/axios';

import './Home.scss';
import { useQuery } from 'react-query';
import Loading from '../../components/Loading';
import NoData from '../../components/NoData';
import ReactHelmet from '../../../../components/ReactHelmet';

const fetchHomeDetail = async () => {
    const data = await axios.get(`/admin/home`)
    return data;
}

const Home = () => {
    const [loading, setLoading] = useState(true);
    const [bestSellingProducts, setBestSellingProducts] = useState([]);
    const [popularProducts, setPopularProducts] = useState([]);
    const [featuredBlogs, setFeaturedBlogs] = useState([]);
    const [featuredProducts, setFeaturedProducts] = useState([]);
    const [testimony, setTestimony] = useState([]);

    const { status } = useQuery('home', fetchHomeDetail, {
        retry: false,
        refetchOnWindowFocus: false,
        onSuccess: (res) => {
            const result = {
                status: res.status + "-" + res.statusText,
                headers: res.headers,
                data: res.data,
            };
            setBestSellingProducts(result.data.bestSellingProducts);
            setPopularProducts(result.data.popularProducts);
            setFeaturedBlogs(result.data.featuredBlogs);
            setFeaturedProducts(result.data.featuredProducts);
            setTestimony(result.data.testimony);
            setLoading(false)
        },
        onError: (res) => {
            setLoading(false)
        }
    });

    useEffect(() => {
        window.scrollTo(0, 0);
    }, [])

    return (
        <div className="home">
            <ReactHelmet
                title="Home"
            />
            {
                loading &&
                <Loading />
            }

            {
                status === "error" &&
                <NoData />
            }

            {
                status === "success" &&
                !loading &&
                <>
                    {/* Category Menu */}
                    <CategoryMenu />

                    {/* Best Selling Products */}
                    <BestSellingProducts
                        bestSellingProducts={bestSellingProducts}
                    />

                    {/* Best From Farmers */}
                    <BestFromFarmers
                        featuredProducts={featuredProducts}
                    />

                    {/* Customer Testimony */}
                    <CustomerTestimony
                        testimony={testimony}
                    />

                    {/* Popular Products */}
                    <PopularProducts
                        popularProducts={popularProducts}
                    />

                    {/* Our Blogs */}
                    <OurBlogs
                        featuredBlogs={featuredBlogs}
                    />
                </>
            }
        </div>
    )
}

export default Home
