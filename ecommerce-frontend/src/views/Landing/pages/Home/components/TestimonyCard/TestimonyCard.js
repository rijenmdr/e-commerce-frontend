import React from 'react'
import { Card, Image } from 'react-bootstrap'

const TestimonyCard = ({ testimony }) => {
    return (
        <Card className="pt-5 testimony-card pb-4 px-5 w-30 position-relative mr-xl">
            <Card.Text>
                <h5 className="text-center">
                    {`“ ${testimony?.quote} “`}
                </h5>
                <h6 className="text-dark-light text-center mt-4">
                    {testimony?.name}
                </h6>
            </Card.Text>
            <Image
                className="position-absolute"
                style={{
                    objectFit: "cover",
                    bottom: -50,
                    left: "50%",
                    transform: "translate(-50%, -50%)",
                }}
                src={testimony?.profileImage}
                width={50}
                height={50}
                roundedCircle
            />
        </Card>
    )
}

export default TestimonyCard
