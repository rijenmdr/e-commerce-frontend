import React from 'react'
import { Button, Col, Row } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'
import HeaderWithList from '../../../../components/HeaderWithList/HeaderWithList'
import ProductCard from '../../../../components/ProductCard/ProductCard'

const best_selling_products = {
    title: "Best selling products",
    list: [
        {
            name: "Kitchen",
            link: `/products?category=61de64f83d339dbb57bad34e`
        },
        {
            name: "Meat and fish",
            link: `/products?category=61de64e63d339dbb57bad34b`
        },
        {
            name: "Technology",
            link: `/products?category=61c5e31c649e325820ffaf1f`
        },
        {
            name: "Food",
            link: `/products?category=61c5e2df649e325820ffaf19`
        },
        {
            name: "Clothes",
            link: "/products?category=61c5e340649e325820ffaf25"
        }
    ]
}

const BestSellingProducts = ({ bestSellingProducts }) => {
    const navigate = useNavigate();
    return (
        <div className="section-margin best-selling-products d-flex justify-content-between">
            <div className="d-flex best-product-header flex-column mr-xl">
                <HeaderWithList
                    title={best_selling_products.title}
                    list={best_selling_products.list}
                />
                <Button
                    onClick={() => navigate('/products')}
                    className="mt-5"
                    variant="secondary"
                >
                    {`More products >`}
                </Button>
            </div>
            <div className="best-product-header-sec">
                <h3 className="">Best Selling Products</h3>
                <h5
                    onClick={() => navigate('/products')}
                    className="cursor-pointer ml-md"
                >
                    View More <span className="text-secondary ml-sm">{'>'}</span>
                </h5>
            </div>
            <Row className="product-list justify-content-between">
                {
                    bestSellingProducts.map(products => (
                        <Col key={products.id} xl="4" lg="6" md="6">
                            <ProductCard
                                key={products?._id}
                                products={products}
                            />
                        </Col>
                    ))
                }
            </Row>
        </div>
    )
}

export default BestSellingProducts;