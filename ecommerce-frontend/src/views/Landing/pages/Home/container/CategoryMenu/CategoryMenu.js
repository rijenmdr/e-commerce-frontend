import React from 'react';
import { Carousel, Button } from 'react-bootstrap';
import HeaderWithList from '../../../../components/HeaderWithList/HeaderWithList';

const category_menu = {
    title: "Category menu",
    list: [
        {
            name: "Bakery",
            link: `/products?category=61de651d3d339dbb57bad351`
        },
        {
            name: "Food",
            link: `/products?category=61c5e2df649e325820ffaf19`
        },
        {
            name: "Meat and fish",
            link: `/products?category=61de64e63d339dbb57bad34b`
        },
        {
            name: "Drinks",
            link: `/products?category=61de63ee3d339dbb57bad344`
        },
        {
            name: "Kitchen",
            link: `/products?category=61de64f83d339dbb57bad34e`
        }
    ]
}

const carousel_list = [
    {
        id: 1,
        img_url: "https://png.pngtree.com/thumb_back/fh260/back_our/20190620/ourmid/pngtree-fashion-food-poster-background-image_159294.jpg"
    },
    {
        id: 2,
        img_url: "https://www.littlewoods.com/assets/static/2021/11/4thways/05-fitbit/fitbit-charge-banner-mb.png"
    },

]

const CategoryMenu = () => {
    return (
        <div className="section-margin home-top-content d-flex justify-content-between">
            <div className="d-flex category-menu flex-column">
                <HeaderWithList
                    title={category_menu.title}
                    list={category_menu.list}
                />
                <Button className="mt-5" variant="secondary">{`More categories >`}</Button>
            </div>
            <Carousel controls={false} className="w-75 corousel-slider">
                {
                    carousel_list.map(list => (
                        <Carousel.Item key={list.id} className="border-4 item-corousel" style={{ height: "350px" }}>
                            <img
                                className="d-block w-100 h-100"
                                src={list.img_url}
                                alt="First slide"
                                style={{
                                    borderRadius: "8px"
                                }}
                            />
                        </Carousel.Item>
                    ))
                }
            </Carousel>
        </div>
    )
}

export default CategoryMenu
