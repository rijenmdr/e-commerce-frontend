import React from 'react'
import { Button, Col, Row } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'
import HeaderWithList from '../../../../components/HeaderWithList/HeaderWithList'
import ProductCard from '../../../../components/ProductCard/ProductCard'

const best_from_farmer = {
    title: "Best from Farmers",
    list: [
        {
            name: "Carrots",
            link: `/products?search=carrots`
        },
        {
            name: "Tomatoes",
            link: `/products?search=tomatoes`
        },
        {
            name: "Potatoes",
            link: `/products?search=potatoes`
        },
        {
            name: "Chicken",
            link: `/products?search=chicken`
        },
        {
            name: "Pork",
            link: `/products?search=pork`
        }
    ]
}

const BestFromFarmers = ({ featuredProducts }) => {
    const navigate = useNavigate();
    return (
        <div className="section-margin best-from-farmer d-flex justify-content-between">
            <div className="d-flex best-from-farmer-header flex-column mr-xl">
                <HeaderWithList
                    title={best_from_farmer.title}
                    list={best_from_farmer.list}
                />
                <Button
                    onClick={() => navigate('/products')}
                    className="mt-5"
                    variant="secondary"
                >
                    {`More products >`}
                </Button>
            </div>
            <div className="best-product-header-sec">
                <h3 className="">Best from farmer</h3>
                <h5 className="cursor-pointer ml-md">View More <span className="text-secondary ml-sm">{'>'}</span></h5>
            </div>
            <Row className="d-flex justify-content-between">
                {
                    featuredProducts &&
                    featuredProducts.length !== 0 &&
                    featuredProducts.map(products => (
                        <Col key={products.id} xl="4" lg="6" md="6">
                            <ProductCard
                                key={products._id}
                                products={products}
                            />
                        </Col>
                    ))
                }
            </Row>
        </div>
    )
}

export default BestFromFarmers
