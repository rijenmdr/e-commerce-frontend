import React from 'react';
import { Col, Row } from 'react-bootstrap';
import ProductCard from '../../../../components/ProductCard/ProductCard';

const PopularProducts = ({ popularProducts }) => {
    return (
        <div className="section-margin popular-products d-flex flex-column">
            <div className="popular-products-header d-flex justify-content-between align-items-center">
                <h3 className="">Popular Products</h3>
                <h5 className="cursor-pointer ml-md">View More <span className="text-secondary ml-sm">{'>'}</span></h5>
            </div>
            <Row className="mt-4 justify-content-between align-items-center">
                {
                    popularProducts.map(products => (
                        <Col key={products.id} xl="3" lg="4" md="6" sm="12" className="mt-2">
                            <ProductCard
                                key={products._id}
                                products={products}
                            />
                        </Col>
                    ))
                }
            </Row>
        </div>
    )
}

export default PopularProducts
