import React from 'react'
import { Col, Row } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import PrimaryBlog from '../../../../components/Blog/PrimaryBlog';
import SecondaryBlog from '../../../../components/Blog/SecondaryBlog';
import TerneryBlog from '../../../../components/Blog/TerneryBlog';

const OurBlogs = ({ featuredBlogs }) => {
    const navigate = useNavigate();
    return (
        <div className="section-margin our-blogs d-flex flex-column mt-5">
            <div className="our-blog-header d-flex justify-content-between align-items-center">
                <h3 className="">Read our Blog posts</h3>
                <h5
                    onClick={() => navigate('/blogs')}
                    className="cursor-pointer ml-md d-flex align-items-baseline"
                >
                    View More
                    <h5 className="text-secondary ml-sm">{'>'}</h5>
                </h5>
            </div>
            {
                featuredBlogs &&
                featuredBlogs.length !== 0 &&
                <Row className="our-blog-list mt-4">
                    {
                        featuredBlogs[0] &&
                        <Col className="our-blog-first" lg="6" md="6">
                            <PrimaryBlog
                                blog={featuredBlogs[0]?.blogId}
                            />
                        </Col>
                    }

                    <Col className='blog-list-second cursor-pointer' lg="3" md="6">
                        {featuredBlogs[1] &&
                            <SecondaryBlog
                                blog={featuredBlogs[1]?.blogId}
                            />
                        }
                    </Col>
                    <Col className='last-row-blogs' lg="3" md="12">
                        {
                            featuredBlogs[2] &&
                            <TerneryBlog
                                blog={featuredBlogs[2]?.blogId}
                            />
                        }
                        {
                            featuredBlogs[3] &&
                            <TerneryBlog
                                blog={featuredBlogs[3]?.blogId}
                            />
                        }
                        {
                            featuredBlogs[4] &&
                            <TerneryBlog
                                blog={featuredBlogs[4]?.blogId}
                            />
                        }
                    </Col>
                </Row>
            }
        </div>
    )
}

export default OurBlogs
