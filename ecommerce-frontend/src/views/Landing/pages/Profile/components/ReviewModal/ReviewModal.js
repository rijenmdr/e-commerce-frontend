import Cookies from 'js-cookie';
import React, { useState } from 'react'
import { Button, Modal, ModalBody, ModalFooter, ModalTitle } from 'react-bootstrap'
import { useQuery } from 'react-query';
import Loading from '../../../../../../components/Loading/Loading';
import axios from '../../../../../../helper/axios';
import NoData from '../../../../components/NoData';
import PurchasedProducts from './PurchasedProducts';
import ReviewProduct from './ReviewProduct';

const fetchAllPurchasedProducts = async () => {
    const data = await axios.get(`/user/all-product-purchases`, {
        headers: {
            Authorization: 'Bearer ' + Cookies.get('token'),
        }
    })
    return data;
}

const ReviewModal = ({ modal, closeModal, refetch }) => {
    const [products, setProducts] = useState([]);
    const [selectedProduct, setSelectedProduct] = useState(null);

    const { status } = useQuery('purchasedProducts', fetchAllPurchasedProducts, {
        retry: false,
        refetchOnWindowFocus: false,
        onSuccess: (res) => {
            const result = {
                status: res.status + "-" + res.statusText,
                headers: res.headers,
                data: res.data,
            };
            setProducts(result.data.purchasedProducts);
        }
    });

    const handleReview = (product) => {
        console.log(product)
        setSelectedProduct(product)
    }

    return (
        <Modal
            centered
            animation
            contentClassName="review-modal"
            show={modal}
            onHide={closeModal}
        >
            <ModalBody>
                {
                    status === "loading" ?
                        <Loading />
                        : status === "error" ?
                            <NoData />
                            : status === "success" && products.length === 0 ?
                                <>
                                    <h4 className='text-center'>Before you review any product you need to purchase it.</h4>
                                </>
                                :
                                <>
                                    {
                                        !selectedProduct ?
                                        <>
                                        <h4>Purchased Products</h4>
                                            {
                                                products &&
                                                products.map(product => (
                                                    <PurchasedProducts
                                                        key={product?._id}
                                                        product={product}
                                                        handleReview={(product) => handleReview(product)}
                                                    />
                                                ))
                                            }
                                        </> :
                                        <>
                                            <ReviewProduct
                                                selectedProduct={selectedProduct}
                                                refetch={refetch}
                                                closeModal={closeModal}
                                                setSelectedProduct={setSelectedProduct}
                                            />
                                        </>
                                    }
                                </>
                }
            </ModalBody>
            {/* <ModalFooter>
                <Button
                    variant="secondary"
                    onClick={closeModal}
                >
                    Cancel
                </Button>
            </ModalFooter> */}
        </Modal>
    )
}

export default ReviewModal