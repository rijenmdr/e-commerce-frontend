import React from 'react'
import { Button, Card, Col, Image, Row } from 'react-bootstrap'
import StarRatings from 'react-star-ratings';
import { ellipse } from '../../../../../../helper/ellipse';

const PurchasedProducts = ({ product, handleReview, isViewing }) => {
    return (
        <Card className='p-2 purchased-products my-2 d-flex flex-row align-items-center justify-content-between'>
            <div className='d-flex align-items-center'>
                <Image
                    src={product?.previewImg}
                    width={100}
                    height={100}
                />
                <div className='px-4'>
                    <h5>{ellipse(product?.name, 20)}</h5>
                    <StarRatings
                        rating={parseFloat(product?.averageRating)}
                        starRatedColor="#FDBC15"
                        starDimension='20'
                        starSpacing='5'
                        numberOfStars={5}
                    />
                </div>
            </div>
            {
                !isViewing &&
                <Button
                    onClick={() => handleReview(product)}
                    size="sm"
                >
                    Add Review
                </Button>
            }
        </Card>
    )
}

export default PurchasedProducts