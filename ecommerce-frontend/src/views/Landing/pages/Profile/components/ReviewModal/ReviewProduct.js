import { Formik } from 'formik'
import React, { useState } from 'react'
import { Button, Col, Form, Row, Spinner } from 'react-bootstrap'
import StarRatings from 'react-star-ratings'
import TextAreaInput from '../../../../components/TextAreaInput/TextAreaInput'
import PurchasedProducts from './PurchasedProducts'
import * as Yup from 'yup';
import { useMutation } from 'react-query'
import { errorResponse } from '../../../../../../helper/errorResponse'
import { toast } from 'react-toastify'
import Cookies from 'js-cookie'
import axios from '../../../../../../helper/axios'

const reviewValidation = Yup.object().shape({
    rating: Yup.number().required("Rating is required").min(1, "You must provide the rating for the product"),
});

const ReviewProduct = ({ selectedProduct, refetch, closeModal, setSelectedProduct }) => {

    const mutation = useMutation(async (postData) => {
        return await axios.post('/product/add-review', postData, {
            headers: {
                'Authorization': 'Bearer ' + Cookies.get('token')
            }
        })
    }, {
        onSuccess: (res) => {
            toast.success("Review Added to product successfully");
            refetch();
            closeModal();
        },
        onError: (res) => {
            console.log(res.response)
            errorResponse(res)
        }
    });

    return (
        <div className='review-product'>
            <h4 className='mb-4'>Add your review</h4>

            <PurchasedProducts
                product={selectedProduct}
                isViewing={true}
            />

            <div className='reviews'>
                <Formik
                    initialValues={{ rating: 0, review: "" }}
                    validationSchema={reviewValidation}
                    onSubmit={(values, { setSubmitting, resetForm }) => {
                        const postData = {
                            productId: selectedProduct?._id,
                            rating: values.rating,
                            review: values.review
                        }
                        mutation.mutate(postData)
                    }}
                >
                    {({
                        values,
                        setValues,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                    }
                    ) => (
                        <Form onSubmit={handleSubmit}>
                            <Row className='mt-4'>
                                <Col xl="12">
                                    <h5 className='mb-0'><b>Rating</b></h5>
                                    <StarRatings
                                        rating={values.rating}
                                        changeRating={(rate) => setValues({
                                            ...values,
                                            rating: rate
                                        })}
                                        starRatedColor="#FDBC15"
                                        starHoverColor='#FDBC15'
                                        starDimension='30'
                                        starSpacing='5'
                                        numberOfStars={5}
                                    />
                                    {touched.rating && errors.rating ? (
                                        <div className="error-message">{errors.rating}</div>
                                    ) : null}
                                </Col>
                            </Row>
                            <Row className='mt-4'>
                                <Col xl="12">
                                    <TextAreaInput
                                        name="review"
                                        label="Review"
                                        value={values.review}
                                        handleChange={handleChange}
                                        row={5}
                                        handleBlur={handleBlur}
                                        placeholder={"Have anything to say ?"}
                                        disabled={mutation.isLoading}
                                        touched={touched.review}
                                        errors={errors.review}
                                        resize={true}
                                    />
                                </Col>
                            </Row>
                            <div className='d-flex justify-content-between mt-2'>
                                <Button
                                    type="submit"
                                    variant='primary'
                                    disabled={mutation.isLoading}
                                >
                                    {mutation.isLoading ?
                                        <Spinner
                                            variant='light'
                                            animation='border'
                                            size='sm'
                                        /> :
                                        'Confirm'
                                    }
                                </Button>
                                <Button
                                    type="button"
                                    variant='secondary'
                                    disabled={mutation.isLoading}
                                    onClick={() => setSelectedProduct(null)}
                                >
                                    Back
                                </Button>
                            </div>
                        </Form>
                    )}
                </Formik>
            </div>
        </div>
    )
}

export default ReviewProduct