import React from 'react'

const ProfileInfo = ({ label, value }) => {
    return (
        <div className='d-flex flex-column'>
            <h5 className='mb-0'><b>{label}</b></h5>
            <h5 className='text-secondary'>{value}</h5>
        </div>
    )
}

export default ProfileInfo