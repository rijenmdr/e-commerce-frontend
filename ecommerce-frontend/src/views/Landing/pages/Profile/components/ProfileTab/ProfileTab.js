import React from 'react'
import { useNavigate } from 'react-router-dom'

const ProfileTab = ({ item, active }) => {
    const navigation = useNavigate();

    return (
        <h5
            onClick={() => navigation(item.link) }
            className={`profile-tab ${active && 'active-tab'}`}
        >
            {item.label}
        </h5>
    )
}

export default ProfileTab