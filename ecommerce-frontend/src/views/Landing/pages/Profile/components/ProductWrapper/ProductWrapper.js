import React from 'react'
import { Card, Col, Image, Row } from 'react-bootstrap'

const ProductWrapper = ({ product, children }) => {
  return (
    <Card className='p-4 product-wrapper mb-2'>
        <Row className='align-items-center'>
            <Col xl="3">
                <Image
                    src={product?.previewImg}
                    alt="preview image"
                    width="100"
                    height="100"
                />
            </Col>
            <Col className='d-flex' xl="3">
                <h4>{product?.name}</h4>
            </Col>
            <Col xl="3">
                <h4 className='text-secondary mb-0'>Rs. {product?.actualPrice}</h4>
                {
                    product?.previousPrice !== 0 &&
                    <h5 className='text-muted mb-0'>
                        <s>Rs. {product?.previousPrice}</s>
                    </h5>
                }
            </Col>
            <Col className='d-flex justify-content-end' lg="3">
                {children}
            </Col>
        </Row>
    </Card>
  )
}

export default ProductWrapper