import { Formik } from 'formik'
import React from 'react'
import { Button, Col, Form, Modal, ModalBody, ModalFooter, ModalTitle, Row } from 'react-bootstrap'
import TextAreaInput from '../../../../components/TextAreaInput/TextAreaInput'
import * as Yup from 'yup';

const additionalInfoValidation = Yup.object().shape({
    reason: Yup.string().required("Order Note is required"),
});

const OrderCancel = ({ modal, closeModal, handleSubmit }) => {
    return (
        <Modal
            centered
            animation
            contentClassName='cancel-content'
            show={modal}
            onHide={closeModal}
        >
            <ModalTitle
                className='p-4'
            >
                <h4>Are you sure you want to cancel order?</h4>
            </ModalTitle>
            <ModalBody>
                <Formik
                    initialValues={{reason:""}}
                    validationSchema={additionalInfoValidation}
                    onSubmit={(values, { setSubmitting, resetForm }) => {
                        handleSubmit(values)
                    }}
                >
                    {({
                        values,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                    }
                    ) => (
                        <Form onSubmit={handleSubmit}>
                            <Row className='mt-4'>
                                <Col xl="12">
                                    <TextAreaInput
                                        name="reason"
                                        label="Reason for cancelation"
                                        value={values.reason}
                                        handleChange={handleChange}
                                        row={5}
                                        handleBlur={handleBlur}
                                        placeholder={"Any Specific reason for cancelation of order"}
                                        // disabled={isLoading}
                                        touched={touched.reason}
                                        errors={errors.reason}
                                        resize={true}
                                    />
                                </Col>


                            </Row>
                            <div className='d-flex justify-content-end mt-4'>
                                <Button
                                    type="button"
                                    variant='secondary'
                                    onClick={closeModal}
                                >
                                    Cancel
                                </Button>
                                <Button
                                    type="submit"
                                    variant='primary'
                                >
                                    Confirm
                                </Button>
                            </div>
                        </Form>
                    )}
                </Formik>
            </ModalBody>
        </Modal>
    )
}
export default OrderCancel