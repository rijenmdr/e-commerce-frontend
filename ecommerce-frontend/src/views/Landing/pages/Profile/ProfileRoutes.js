import React from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'
import Purchases from './container/Purchases/Purchases'
import Reviews from './container/Reviews/Reviews'
import Wishlist from './container/Wishlist/Wishlist'

const Account = React.lazy(() => 
  import('./container/Account/Account')
);

const ProfileRoutes = () => {
  return (
    <Routes>
        <Route path='/account' name="account" element={<Account/>} />
        <Route path='/purchases' name="purchase" element={<Purchases/>} />
        <Route path='/reviews' name="reviews" element={<Reviews/>} />
        <Route path='/wishlist' name="wishlist" element={<Wishlist/>} />   
        <Route path ='*' element={<Navigate to="/profile/account" />} />     
    </Routes>
  )
}

export default ProfileRoutes