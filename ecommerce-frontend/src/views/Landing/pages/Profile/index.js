import React from 'react';
import { Card, Col, Row } from 'react-bootstrap';
import Breadcrumbs from '../../container/Breadcrumb';
import ProfileTab from './components/ProfileTab/ProfileTab';
import ReactHelmet from '../../../../components/ReactHelmet';

import { useLocation } from 'react-router-dom';

import './Profile.scss';
import ProfileRoutes from './ProfileRoutes';
import { useEffect } from 'react';

const sidebarItems = [
    {
        id: 1,
        label: "Account",
        link: '/profile/account'
    },
    {
        id: 2,
        label: "My Purchases",
        link: '/profile/purchases'
    },
    {
        id: 3,
        label: "My Reviews",
        link: '/profile/reviews'
    },
    {
        id: 4,
        label: "My Wishlist",
        link: '/profile/wishlist'
    }
]

const Profile = () => {
    const { pathname } = useLocation();

    useEffect(() => {
        window.scrollTo(0, 200);
    }, [])
    

    return (
        <div className='profile'>
            <ReactHelmet
                title="Profile"
            />
            <Breadcrumbs />
            <Row className='section-margin'>
                <Col className='d-flex flex-column overflow-hidden' xl="3">
                    {
                        sidebarItems.map(item => (
                            <ProfileTab
                                active={item.link === pathname}
                                key={item.id}
                                item={item}
                            />
                        ))
                    }
                </Col>
                <Col xl="8">
                    <Card className='p-4'>
                        <ProfileRoutes/>
                    </Card>
                </Col>
            </Row>
        </div>
    )
}

export default Profile