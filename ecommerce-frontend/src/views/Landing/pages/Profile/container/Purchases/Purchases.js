import Cookies from 'js-cookie';
import React, { useState } from 'react';
import { useQuery } from 'react-query';
import Loading from '../../../../../../components/Loading/Loading';
import NoData from '../../../../components/NoData';
import axios from '../../../../../../helper/axios';
import PurchaseDetail from './PurchaseDetail';
import { Card } from 'react-bootstrap';

const fetchUserPurchases = async () => {
  const data = await axios.get(`/user/product-purchases`, {
    headers: {
      Authorization: 'Bearer ' + Cookies.get('token'),
    }
  })
  return data;
}

const Purchases = () => {
  const [purchases, setPurchases] = useState([]);

  const { status } = useQuery('blogs', fetchUserPurchases, {
    retry: false,
    refetchOnWindowFocus: false,
    onSuccess: (res)=>{
        const result = {
            status: res.status + "-" + res.statusText,
            headers: res.headers,
            data: res.data,
          };
        setPurchases(result.data.purchases);
    }
});

  return (
    <div className='purchases'>
      {
        status === "loading" ?
          <Loading/> 
        : status === "error" ?
          <NoData/>
        : status === "success" && purchases.length === 0 ?
          <>
            <h4>Please make some purchases</h4> 
            <NoData/>
          </>
        : 
        <>
          <h3 className='mb-5'>Purchase Detail</h3>
          <PurchaseDetail
            purchases={purchases}
          />
        </>
      }
    </div>
  )
}

export default Purchases