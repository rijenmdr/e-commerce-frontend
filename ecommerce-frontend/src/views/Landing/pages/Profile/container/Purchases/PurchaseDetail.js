import React, { useState } from 'react'
import { Button, Image, Table } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { setModal } from '../../../../../../redux/actions'
import OrderCancel from '../../components/OrderCancel/OrderCancel'

const PurchaseDetail = ({ purchases }) => {
  const [selectedProductId, setSelectedProductId] = useState(false);
  
  const modal = useSelector(state => state.application.modal);

  const dispatch = useDispatch();

  const handleModal = () => {
    dispatch(setModal(true));
  }
  console.log(modal)

  return (
    <div className='purchase-detail overflow-auto'>
      <Table responsive striped hover bordered>
        <thead className='table-header'>
          <tr className='header-row'>
            <th>SN</th>
            <th>Products</th>
            <th>Payment Method</th>
            <th>Expected Date</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {
            purchases &&
            purchases.length !== 0 &&
            purchases.map((purchase, index) => (
              <tr
                key={purchase?._id}
              >
                <td><h5>{index + 1}</h5></td>
                <td>{purchase?.productIds?.map(product => (
                  <div
                    key={product?._id}
                    className='d-flex align-items-center p-2'
                  >
                    <Image
                      src={product?.previewImg}
                      width="50"
                      height="50"
                      alt="preview image"
                    />
                    <span className='px-2'><h5>{product?.name}</h5></span>
                  </div>
                ))}
                </td>
                <td><h5 className='text-center py-2'>{!purchase?.hasPaid ? 'Cash on Delivery' : 'Online Payment'}</h5></td>
                <td><h5 className='text-center py-2'>{purchase?.expectedDate}</h5></td>
                <td>
                  <Button
                    type='button'
                    size='sm'
                    variant='danger'
                    onClick={handleModal}
                  >
                    Cancel
                  </Button>
                </td>
              </tr>
            ))
          }
        </tbody>
      </Table>

      <OrderCancel
        modal={modal}
        closeModal={() => dispatch(setModal(false))}
      />

    </div>
  )
}

export default PurchaseDetail;