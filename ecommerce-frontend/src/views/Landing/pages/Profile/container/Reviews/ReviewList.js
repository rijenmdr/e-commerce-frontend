import React, { useState } from 'react'
import { Button, Image, Table } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import StarRatings from 'react-star-ratings'
import { setModal } from '../../../../../../redux/actions'
import OrderCancel from '../../components/OrderCancel/OrderCancel'

const ReviewList = ({ reviews }) => {
  const [selectedProductId, setSelectedProductId] = useState(false);
  
  const modal = useSelector(state => state.application.modal);

  const dispatch = useDispatch();

  return (
    <div className='review-detail overflow-auto'>
      <Table responsive striped hover bordered>
        <thead className='table-header'>
          <tr className='header-row'>
            <th>SN</th>
            <th>Products</th>
            <th>Review</th>
            <th>Rating</th>
          </tr>
        </thead>
        <tbody>
          {
            reviews &&
            reviews.length !== 0 &&
            reviews.map((review, index) => (
              <tr
                key={review?._id}
              >
                <td><h5>{index + 1}</h5></td>
                <td>
                  <div
                    className='d-flex align-items-center p-2'
                  >
                    <Image
                      src={review?.productId?.previewImg}
                      width="50"
                      height="50"
                      alt="preview image"
                    />
                    <span className='px-2'><h5>{review?.productId?.name}</h5></span>
                  </div>
                </td>
                <td><h6 className='py-2'>{review?.review}</h6></td>
                <td>
                    <StarRatings
                        rating={parseFloat(review?.rating)}
                        starRatedColor="#FDBC15"
                        starDimension='20'
                        starSpacing='5'
                        numberOfStars={5}
                    />
                </td>
              </tr>
            ))
          }
        </tbody>
      </Table>

    </div>
  )
}

export default ReviewList;