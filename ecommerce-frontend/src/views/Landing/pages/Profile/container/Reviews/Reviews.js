import Cookies from 'js-cookie';
import React, { useState } from 'react';
import { useQuery } from 'react-query';
import Loading from '../../../../../../components/Loading/Loading';
import NoData from '../../../../components/NoData';
import axios from '../../../../../../helper/axios';
import ReviewList from './ReviewList';
import { Button } from 'react-bootstrap';

import { setModal } from '../../../../../../redux/actions'
import ReviewModal from '../../components/ReviewModal/ReviewModal';
import { useSelector,useDispatch } from 'react-redux';

const fetchUserReviews = async () => {
  const data = await axios.get(`/user/reviews`, {
    headers: {
      Authorization: 'Bearer ' + Cookies.get('token'),
    }
  })
  return data;
}

const Reviews = () => {
  const [reviews, setReviews] = useState([]);

  const modal = useSelector(state => state.application.modal);

  const dispatch = useDispatch();

  const { status, refetch } = useQuery('blogs', fetchUserReviews, {
    retry: false,
    refetchOnWindowFocus: false,
    onSuccess: (res)=>{
        const result = {
            status: res.status + "-" + res.statusText,
            headers: res.headers,
            data: res.data,
          };
        setReviews(result.data.reviews);
    }
});

  return (
    <div className='reviews'>
      <div className='d-flex justify-content-between align-items-center mb-5'>  
        <h3 className=''>My Reviews</h3>
        <Button
          type="button"
          onClick={() => dispatch(setModal(true))}
        >
          Add Review
        </Button>
      </div>
      {
        status === "loading" ?
          <Loading/> 
        : status === "error" ?
          <NoData/>
        : status === "success" && reviews.length === 0 ?
          <>
            <h4 className='text-center'>Please make some reviews</h4> 
            {/* <NoData/> */}
          </>
        : 
        <>
          <ReviewList
            reviews={reviews}
          />
        </>
      }

      <ReviewModal
        modal={modal}
        closeModal={() => dispatch(setModal(false))}
        refetch={refetch}
      />

    </div>
  )
}

export default Reviews