import React, { useState } from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { mobileNoValidation } from '../../../../../../helper/validation';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Col, Form, Row, Spinner } from 'react-bootstrap';
import TextInput from '../../../../components/TextInput/TextInput';
import SelectInputForm from '../../../../components/SelectInput/SelectInputForm';
import { useMutation } from 'react-query';
import axios from '../../../../../../helper/axios';
import { toast } from 'react-toastify';
import { errorResponse } from '../../../../../../helper/errorResponse';
import { loginSuccess } from '../../../../../../redux/actions';
import Cookies from 'js-cookie';

const billingInfoValidation = Yup.object().shape({
  name: Yup.string().required("First Name is required"),
  email: Yup.string()
    .required("Email is required")
    .email("*Email Address is invalid"),
  gender: Yup.string().required("Gender is required"),
  dob: Yup.string().required("Date of Birth is required"),
  street: Yup.string().required("Street is required"),
  city: Yup.string().required("City is required"),
  country: Yup.string().required("Country is required"),
});


const EditAccountDetail = ({ setIsEditing }) => {
  const selectOptions = [
    {
      id: 1,
      label: "Male",
      value: "m",
    },
    {
      id: 2,
      label: "Female",
      value: "f",
    },
    {
      id: 3,
      label: "Other",
      value: "o",
    },
  ]

  const user = useSelector(state => state.user.currentUser);

  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState({
    name: user?.name,
    email: user?.email,
    street: user?.address?.street,
    city: user?.address?.city,
    country: user?.address?.country,
    dob: user?.dob?.split('T')[0],
    gender: user?.gender,
  });

  const dispatch = useDispatch();

  const mutation = useMutation(async (postData) => {
    return await axios.post('/user/update-profile', postData,{
      headers: {
        Authorization: 'Bearer ' + Cookies.get('token'),
      }
    })
  }, {
    onSuccess: (res) => {
      const result = {
        status: res.status + "-" + res.statusText,
        headers: res.headers,
        data: res.data,
      };
      dispatch(loginSuccess(result.data?.currentUser));
      toast.success(`User profile updated successfully`);
      setIsEditing(false)
      setIsLoading(false);
    },
    onError: (res) => {
      errorResponse(res)
      setIsLoading(false)
    }
  });

  return (
    <div className='account-detail'>
      <h3 className='mb-5'>Account Detail</h3>
      <Formik
        initialValues={data}
        validationSchema={billingInfoValidation}
        onSubmit={(values, { setSubmitting, resetForm }) => {
          setIsLoading(true);
          const postData = {
            name: values.name,
            email: values.email,
            gender: values.gender,
            dob: values.dob,
            street: values.street,
            city: values.city,
            country: values.country
          }
          mutation.mutate(postData)
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
        }
        ) => (
          <Form onSubmit={handleSubmit}>
            <Row className='mt-4'>
              <h4 className='mb-3'>Personal Information</h4>
              <Col xl="6" lg="6">
                <TextInput
                  name="name"
                  label="Name"
                  type="text"
                  value={values.name}
                  handleChange={handleChange}
                  handleBlur={handleBlur}
                  placeholder={"Enter your Name"}
                  disabled={isLoading}
                  touched={touched.name}
                  errors={errors.name}
                />
              </Col>
              <Col xl="6" lg="6">
                <TextInput
                  name="email"
                  label="Email"
                  type="text"
                  value={values.email}
                  handleChange={handleChange}
                  handleBlur={handleBlur}
                  placeholder={"Enter your Email"}
                  disabled={isLoading}
                  touched={touched.email}
                  errors={errors.email}
                />
              </Col>
            </Row>
            <Row>
              <Col xl="6" lg="6">
                <SelectInputForm
                  selectOptions={selectOptions}
                  name="gender"
                  label="Gender"
                  value={values.gender}
                  handleChange={handleChange}
                  handleBlur={handleBlur}
                  disabled={isLoading}
                  touched={touched.gender}
                  errors={errors.gender}

                />
              </Col>
              <Col xl="6" lg="6">
                <TextInput
                  name="dob"
                  label="Date of Birth"
                  type="date"
                  value={values.dob}
                  handleChange={handleChange}
                  handleBlur={handleBlur}
                  placeholder={"Enter your Date of Birth"}
                  disabled={isLoading}
                  touched={touched.dob}
                  errors={errors.dob}
                />
              </Col>
            </Row>
            <Row>
              <h4 className='my-3'>Address Information</h4>
              <Col xl="6" lg="6">
                <TextInput
                  name="street"
                  label="Street"
                  type="text"
                  value={values.street}
                  handleChange={handleChange}
                  handleBlur={handleBlur}
                  placeholder={"Enter your Street"}
                  disabled={isLoading}
                  touched={touched.street}
                  errors={errors.street}
                />
              </Col>

              <Col xl="6" lg="6">
                <TextInput
                  name="city"
                  label="City"
                  type="text"
                  value={values.city}
                  handleChange={handleChange}
                  handleBlur={handleBlur}
                  placeholder={"Enter your City"}
                  disabled={isLoading}
                  touched={touched.city}
                  errors={errors.city}
                />
              </Col>
            </Row>
            <Row>
              <Col xl="12">

                <TextInput
                  name="country"
                  label="Country"
                  type="text"
                  value={values.country}
                  handleChange={handleChange}
                  handleBlur={handleBlur}
                  placeholder={"Enter your Country"}
                  disabled={isLoading}
                  touched={touched.country}
                  errors={errors.country}
                />
              </Col>
            </Row>
            <div className='d-flex justify-content-between mt-4'>
              <Button
                onClick={() => setIsEditing(false)}
                type="button"
                disabled={isLoading}
                variant='secondary'
              >
                Back
              </Button>
              <Button
                type="submit"
                variant='primary'
                disabled={isLoading}
              >
                {
                  isLoading ?
                    <Spinner
                      variant='light'
                      animation='border'
                      size='sm'
                    /> :
                    'Update Profile'
                }
              </Button>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  )
}

export default EditAccountDetail