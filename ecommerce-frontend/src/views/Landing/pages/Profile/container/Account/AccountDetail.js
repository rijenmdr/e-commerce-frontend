import moment from 'moment';
import React, { useRef } from 'react';
import { Col, Image, Row } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import ProfileInfo from '../../components/ProfileInfo/ProfileInfo';
import NoData from '../../../../components/NoData';
import { errorResponse } from '../../../../../../helper/errorResponse';
import { initialUser, loginSuccess, userError } from '../../../../../../redux/user/actions';
import axios from '../../../../../../helper/axios';
import Cookies from 'js-cookie';
import Loading from '../../../../components/Loading';

const AccountDetail = ({ setIsEditing }) => {

    const { currentUser, loading, error } = useSelector(state => state.user);

    const fileRef = useRef();

    const dispatch = useDispatch();

    const handleFileChange = async(e) => {
        const file = e.target.files[0];

        let reader = new FileReader();
        reader.readAsDataURL(file);

        reader.onload = async function () {
            dispatch(initialUser())
            try {
                const user = await axios.post('/user/upload-avatar', {
                    image: reader.result
                }, {
                    headers: {
                        Authorization: 'Bearer ' + Cookies.get('token'),
                    }
                });
                dispatch(loginSuccess(user.data.currentUser));
            } catch(err) {
                errorResponse(err);
                dispatch(userError())
            }
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }

    console.log(currentUser)

    return (
        <div className='account-detail'>
            {
                loading ?
                    <Loading/> :
                error ?
                    <NoData/> :
                    <>
                        <h3 className='mb-5'>Account Detail</h3>
                        <Row>
                            <Col className='d-flex flex-column' xl="4" lg="4" md="4">
                                <input
                                    type="file"
                                    hidden
                                    ref={fileRef}
                                    onChange={(e) => handleFileChange(e)}
                                />
                                <div 
                                    onClick={() => fileRef.current.click()}
                                    className='image'
                                >
                                    <Image
                                        src={currentUser?.profileImage ? currentUser.profileImage : 'https://freepikpsd.com/file/2019/10/default-profile-image-png-1-Transparent-Images.png'}
                                        roundedCircle
                                        className='profile-avatar cursor-pointer'
                                    />
                                </div>
                                <h5
                                    onClick={() => setIsEditing(true)}
                                    className='text-secondary m-3 cursor-pointer'
                                >
                                    Edit profile
                                </h5>
                            </Col>
                            <Col xl="4" lg="4" md="4">
                                <h4 className='mb-4'>Personal Detail</h4>
                                <ProfileInfo
                                    label="Name"
                                    value={currentUser?.name}
                                />
                                <ProfileInfo
                                    label="Email"
                                    value={currentUser?.email}
                                />
                                {
                                    currentUser?.mobileNo &&
                                    <ProfileInfo
                                        label="Mobile Number"
                                        value={currentUser?.mobileNo}
                                    />
                                }
                                {
                                    currentUser?.dob &&
                                    <ProfileInfo
                                        label="DOB"
                                        value={moment(currentUser?.dob).format('ll')}
                                    />
                                }
                                {
                                    currentUser?.gender &&
                                    <ProfileInfo
                                        label="DOB"
                                        value={currentUser?.gender === "m" ? 'Male' : currentUser?.gender === "f" ? "Female" : 'Other'}
                                    />
                                }
                            </Col>
                            {
                                currentUser?.address &&
                                Object.entries(currentUser?.address).length !== 0 &&
                                <Col xl="4" lg="4" md="4">
                                    <h4 className='mb-4'>Address Detail</h4>
                                    <ProfileInfo
                                        label="Street"
                                        value={currentUser?.address?.street}
                                    />
                                    <ProfileInfo
                                        label="City"
                                        value={currentUser?.address?.city}
                                    />
                                    <ProfileInfo
                                        label="Country"
                                        value={currentUser?.address?.country}
                                    />
                                </Col>
                            }
                        </Row>
                    </>
            }
        </div>
    )
}

export default AccountDetail