import React, { useEffect, useState } from 'react'
import { Card } from 'react-bootstrap';
import AccountDetail from './AccountDetail';
import EditAccountDetail from './EditAccountDetail';

const Account = () => {
  const [isEditing, setIsEditing] = useState(false);

  useEffect(() => {
    window.scrollTo(0, 200);
  }, [isEditing])
  
  return (
    <div className='account'>
      {
        !isEditing ?
          <AccountDetail
            setIsEditing={setIsEditing}
          /> :
          <EditAccountDetail
            setIsEditing={setIsEditing}
          />
      }
    </div>
  )
}

export default Account