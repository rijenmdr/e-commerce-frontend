import Cookies from 'js-cookie';
import React, { useState } from 'react';
import { useMutation, useQuery } from 'react-query';
import Loading from '../../../../../../components/Loading/Loading';
import NoData from '../../../../components/NoData';
import axios from '../../../../../../helper/axios';
import ProductWrapper from '../../components/ProductWrapper/ProductWrapper';
import { Button, Image, Spinner } from 'react-bootstrap';

import { AiFillDelete } from 'react-icons/ai';
import { FiShoppingCart } from 'react-icons/fi';
import { toast } from 'react-toastify';
import { useDispatch, useSelector } from 'react-redux';
import { errorResponse } from '../../../../../../helper/errorResponse';
import { setModal, updateCart } from '../../../../../../redux/actions';
import ConfirmModal from '../../../../../../components/ConfirmModal/ConfirmModal';

const fetchUserWishlist = async () => {
  const data = await axios.get(`/user/my-wishlist`, {
    headers: {
      Authorization: 'Bearer ' + Cookies.get('token'),
    }
  })
  return data;
}

const Wishlist = () => {
  const [wishlist, setWishlist] = useState([]);
  const [selectedIndex, setSelectedIndex] = useState(null);

  const modal = useSelector(state => state.application.modal);

  console.log(modal)

  const dispatch = useDispatch();

  const { status, refetch } = useQuery('blogs', fetchUserWishlist, {
    retry: false,
    refetchOnWindowFocus: false,
    onSuccess: (res) => {
      const result = {
        status: res.status + "-" + res.statusText,
        headers: res.headers,
        data: res.data,
      };
      setWishlist(result.data.wishlist);
    }
  });

  const cartMutation = useMutation(async (product) => {
    await setSelectedIndex(product?._id);
    console.log(product, selectedIndex)
    const postData = {
      productId: product?._id,
      numberOfItem: 1
    }
    return await axios.post('/product/add-to-cart', postData, {
      headers: {
        'Authorization': 'Bearer ' + Cookies.get('token')
      }
    })
  }, {
    onSuccess: (res) => {
      toast.success("Product added to cart successfully");
      dispatch(updateCart(res.data.cart));
    },
    onError: (res) => {
      console.log(res.response)
      errorResponse(res)
    }
  });

  const removeMutation = useMutation(async () => {
    dispatch(setModal(false));
    return await axios.post('/product/remove-from-favourite', {
      productId: selectedIndex
    }, {
      headers: {
        'Authorization': 'Bearer ' + Cookies.get('token')
      }
    })
  }, {
    onSuccess: (res) => {
      toast.success("Product removed from wishlist successfully");
      refetch();
    },
    onError: (res) => {
      console.log(res.response)
      errorResponse(res)
    }
  });

  const handleModal = (productId) => {
    setSelectedIndex(productId);
    dispatch(setModal(true));
  }

  return (
    <div className='wishlist'>
      {
        status === "loading" ?
          <Loading />
          : status === "error" ?
            <NoData />
            : status === "success" && wishlist.length === 0 ?
              <>
                <h4 className='text-center'>Wishlist is empty. Add some products to wishlist.</h4>
                <NoData />
              </>
              :
              <>
                <h3 className='mb-5'>
                  My Wishlist
                </h3>
                <div
                  className='overflow-auto'
                  style={{ height: "80vh" }}
                >
                  {
                    wishlist &&
                    wishlist.map((list, index) => (
                      <ProductWrapper
                        product={list}
                        key={list?._id}
                      >
                        <Button
                          variant='primary'
                          size='sm'
                          type='button'
                          disabled={cartMutation.isLoading}
                          onClick={() => cartMutation.mutate(list)}
                        >
                          {
                            cartMutation.isLoading && list?._id === selectedIndex ?
                              <Spinner
                                variant='light'
                                animation='border'
                                size='sm'
                              /> :
                              <FiShoppingCart
                                size={20}
                              />
                          }
                        </Button>
                        <Button
                          variant='danger'
                          type='button'
                          disabled={removeMutation.isLoading}
                          onClick={() => handleModal(list?._id)}
                        >
                          {
                            removeMutation.isLoading && list?._id === selectedIndex ?
                              <Spinner
                                variant='light'
                                animation='border'
                                size='sm'
                              /> :
                              <AiFillDelete
                                size="20"
                              />
                          }
                        </Button>
                      </ProductWrapper>
                    ))
                  }
                </div>
              </>
      }
      <ConfirmModal
        modal={modal}
        closeModal={() => dispatch(setModal(false))}
        handleSubmit={removeMutation.mutate}
      />
    </div>
  )
}

export default Wishlist