import React, { useState } from 'react'
import { Button, Col, Form, Row, Spinner } from 'react-bootstrap';
import { useMutation, useQuery } from 'react-query';
import { Link } from 'react-router-dom';
import axios from '../../../../../../helper/axios';
import Loading from '../../../../components/Loading';
import NoData from '../../../../components/NoData';

import TextAreaInput from '../../../../components/TextAreaInput/TextAreaInput';
import ProductQuestionAnswer from '../../components/ProductQuestionAnswer/ProductQuestionAnswer';

import * as Yup from 'yup';
import { toast } from 'react-toastify';
import { Formik } from 'formik';
import Cookies from 'js-cookie';
import { useSelector } from 'react-redux';

const fetchProductQuestionAnswer = async (parameter) => {
    console.log(parameter.queryKey)
    const [key, productId, limit] = parameter.queryKey
    console.log(key)
    const data = await axios.get(`/product/get-product-question/${productId}?limit=${limit}`)
    return data;
}

const questionValidation = Yup.object().shape({
    question: Yup.string()
        .required("*Question is required")
});

const ProductQuestions = ({ productId, product, setQuestionCount, questionCount }) => {
    const [limit, setLimit] = useState(5);
    const [qa, setQA] = useState([]);
    const [refetching, setRefetching] = useState(false);
    const [loading, setLoading] = useState(false);

    const id = useSelector(state => state.user?.currentUser?.id);

    const { status, refetch } = useQuery(['productQ/A', productId, limit], fetchProductQuestionAnswer, {
        enabled: refetching ? false : !!productId,
        retry: false,
        refetchOnWindowFocus: false,
        onSuccess: (res) => {
            const result = {
                status: res.status + "-" + res.statusText,
                headers: res.headers,
                data: res.data,
            };
            setQA(result.data.questionAnswer);
            setQuestionCount(result.data.totalQuestion);
            setRefetching(false);
        }
    });

    const mutation = useMutation(async (postData) => {
        console.log(postData)
        return await axios.post('/product/add-question', postData, {
            headers: {
                'Authorization': 'Bearer ' + Cookies.get('token')
            }
        })
    }, {
        onSuccess: (res) => {
            refetch();
            toast.success("Question added successfully");
            setLoading(false);
        },
        onError: () => {
            toast.error("Error while adding Question");
            setLoading(false)
        }
    });
    return (
        <div className='product-questions w-100'>
            {
                status === "loading" &&
                <Loading />
            }
            {
                status === "error" &&
                <NoData />
            }
            {
                status === "success" &&
                <>
                    <div className='product-questions-header'>
                        {
                            !Cookies.get("token") ?
                                <h5><Link to="/user/login">Login</Link> or <Link to="/user/register">Register</Link> to ask question</h5>
                                :
                                <>
                                    {
                                        id !== product?.merchantId?._id ?
                                            <Formik
                                                initialValues={{ question: "" }}
                                                validationSchema={questionValidation}
                                                onSubmit={async (values, { setSubmitting, resetForm }) => {
                                                    setLoading(true);
                                                    setRefetching(true);
                                                    const postData = {
                                                        question: values.question,
                                                        productId
                                                    }
                                                    await mutation.mutate(postData);
                                                    resetForm();
                                                }}
                                            >
                                                {({
                                                    values,
                                                    errors,
                                                    touched,
                                                    handleChange,
                                                    handleBlur,
                                                    handleSubmit,
                                                }
                                                ) => (
                                                    <Form onSubmit={handleSubmit}>
                                                        <Row className='w-100 align-items-center'>
                                                            <Col lg="9">
                                                                <TextAreaInput
                                                                    name="question"
                                                                    label='Ask any question about the product'
                                                                    row={5}
                                                                    value={values.question}
                                                                    handleChange={handleChange}
                                                                    handleBlur={handleBlur}
                                                                    disabled={loading}
                                                                    placeholder='Ask any Question'
                                                                    touched={touched.question}
                                                                    errors={errors.question}
                                                                />
                                                            </Col>
                                                            <Col lg="3">
                                                                <Button
                                                                    type="submit"
                                                                    size="sm"
                                                                    disabled={loading}
                                                                >
                                                                    {
                                                                        loading ?
                                                                            <Spinner
                                                                                variant='light'
                                                                                animation='border'
                                                                                size='sm'
                                                                            /> :
                                                                            'Ask Question'
                                                                    }
                                                                </Button>
                                                            </Col>
                                                        </Row>
                                                    </Form>
                                                )}
                                            </Formik>
                                            :
                                            <h5>You can leave reply to ask question</h5>
                                    }
                                </>
                        }
                    </div>
                    <div className='divider'></div>
                    {
                        qa.length === 0 ?
                            <NoData /> :
                            <div className='product-question-list mt-4'>
                                <h3>Questions and Answer</h3>
                                <Row className='w-100 mt-4'>
                                    {
                                        qa &&
                                        qa.length !== 0 &&
                                        qa.map(questionAnswer => (
                                            <ProductQuestionAnswer
                                                product={product}
                                                questionAnswer={questionAnswer}
                                                setRefetching={setRefetching}
                                                refetch={refetch}
                                            />
                                        ))
                                    }
                                    {
                                        qa.length !== questionCount &&
                                        <h5
                                            onClick={() => setLimit(prevVal => prevVal + 5)}
                                            className="cursor-pointer ml-md mt-4 text-center"
                                        >
                                            View More
                                        </h5>
                                    }
                                </Row>
                            </div>
                    }
                </>
            }
        </div>
    )
}

export default ProductQuestions
