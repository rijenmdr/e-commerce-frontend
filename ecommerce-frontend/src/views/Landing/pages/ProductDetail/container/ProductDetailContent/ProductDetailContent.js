import React, { useState } from 'react'
import { Button, Card, Col, Image, Row, Spinner } from 'react-bootstrap'
import StarRatings from 'react-star-ratings'
import ProductExtraInfo from '../../components/ProductExtraInfo/ProductExtraInfo'
import SelectNoOfItem from '../../components/SelectNoOfItem/SelectNoOfItem';
import axios from '../../../../../../helper/axios';
import { useNavigate } from 'react-router-dom';
import { useMutation } from 'react-query';
import Cookies from 'js-cookie';
import { toast } from 'react-toastify';
import { errorResponse } from '../../../../../../helper/errorResponse';
import { addProductToFavourite } from '../../../../../../helper/utils';
import { useDispatch, useSelector } from 'react-redux';
import { updateCart } from '../../../../../../redux/actions';

//images
import Heart from '../../../../../../assets/images/icons/ic-actions-heart-red.svg';
import Compare from '../../../../../../assets/images/icons/ic-layout-picture-red-right.svg';


const ProductDetailContent = ({ product }) => {
    const navigation = useNavigate();

    const [numberOfItem, setNumberOfItem] = useState(1);
    const [loading, setLoading] = useState(false);
    const [favLoading, setFavLoading] = useState(false);

    const cart = useSelector(state => state.user.cart); 

    const dispatch = useDispatch();

    const mutation = useMutation(async () => {
        setLoading(true);
        const postData = {
            productId: product?._id,
            numberOfItem: numberOfItem
        }
        return await axios.post('/product/add-to-cart', postData, {
            headers: {
                'Authorization': 'Bearer ' + Cookies.get('token') 
            }
        })
    }, {
        onSuccess: (res) => {
            toast.success("Product added to cart successfully");
            dispatch(updateCart(res.data.cart));
            setLoading(false);
        },
        onError: (res) => {
            console.log(res.response)
            errorResponse(res)
            setLoading(false)
        }
    });
    
    const favouriteMutation = useMutation(async (productId) => {
        setFavLoading(true);
        await addProductToFavourite(productId)
    }, {
        onSuccess: (res) => {
            toast.success("Product added to favourite successfully");
            setFavLoading(false);
        },
        onError: (res) => {
            errorResponse(res)
            setFavLoading(false)
        }
    });
    
    const addProductToCart = () => {
        if(Cookies.get("token")){
            return cart.length >= 10 ? toast.error("Cart is full") : mutation.mutate();
        } 
        toast.warning("Please login to continue")
        navigation('/user/login')
    } 
    
    const addToFavourite = (productId) => {
        if(Cookies.get("token")){
            setFavLoading(true);
            return favouriteMutation.mutate(productId);
        } else {
            toast.warning("Please login to continue")
            navigation('/user/login')
        }
    } 

    return (
        <div className='product-detail-content'>
            <h1>{product?.name}</h1>
            <div className='d-flex align-items-end'>
                <StarRatings
                    rating={parseFloat(product?.averageRating) || 0}
                    starRatedColor="#FDBC15"
                    starDimension='20'
                    starSpacing='5'
                    numberOfStars={5}
                />
                <p className="text-dark-light ml-sm mb-0">
                    <u>({product?.reviews && product?.reviews?.length} customer review)</u>
                </p>
            </div>
            <h5 className='product-short-description mt-5'>
                {product?.shortDescription}
            </h5>
            <Row className="product-extra-detail mt-5">
                <Col className='d-flex flex-column' lg="6">
                    <ProductExtraInfo
                        title="Product Id"
                        value={product?.productId}
                    />
                    <ProductExtraInfo
                        title="Category"
                        value={product?.categoryId?.name}
                    />
                    <ProductExtraInfo
                        title="Stock"
                        value={`${product?.onStock !== 0 ? 'In Stock' : 'Out of Stock'}`}
                    />
                    <ProductExtraInfo
                        title="Delivery"
                        value={`in ${product?.deliveryIn?.numberForDelivery} ${product?.deliveryIn?.timeForDelivery}`}
                    />
                </Col>
                <Col className='d-flex flex-column' lg="6">
                    {
                        product?.extraInformation &&
                        product?.extraInformation.length !== 0 &&
                        product?.extraInformation.map(detail => (
                            Object.keys(detail).map(det => (
                                <ProductExtraInfo
                                    title={det}
                                    value={detail[det]}
                                    key={det}
                                />
                            ))
                        ))
                    }
                </Col>
            </Row>
            <Card className='p-3 d-flex flex-row align-items-center mt-4 justify-content-between flex-wrap'>
                <div className='product-prices'>
                    <h2 className='mb-0 text-secondary'>NRp. {product?.actualPrice}</h2>
                    {
                        product?.previousPrice !== 0 &&
                        <h5 className='mb-0 text-dark-light'>
                            <s>Nrp. {product?.previousPrice}</s>
                        </h5>
                    }
                </div>
                <div className='d-flex add-to-cart align-items-center flex-wrap'>
                    <div>
                        <SelectNoOfItem
                            onStock={product?.onStock}
                            soldIn={product?.soldIn}
                            value={numberOfItem}
                            onChange={(e) => setNumberOfItem(e.target.value)}
                        />
                    </div>
                    <Button
                        onClick={addProductToCart}
                        className='ml-md'
                        size='sm'
                    >
                        {
                            loading ?
                                <Spinner
                                    variant='light'
                                    animation='border'
                                    size='sm'
                                /> :
                                '+ Add to cart'
                        }
                    </Button>
                </div>
            </Card>
            <div className='product-action-options d-flex flex-wrap align-items-center mt-5'>
                <div 
                    onClick={() => addToFavourite(product?._id)}
                    className='add-to-wishlist d-flex align-items-center cursor-pointer'
                >
                    {
                        favLoading ? 
                        <Spinner
                            variant='primary'
                            animation='border'
                            size='sm'
                        /> :
                        <Image
                            src={Heart}
                            alt="heart-icon"
                            width={20}
                            className='svg-icon'
                        />
                    }
                    <h4 className='mb-0 ml-sm'>Add to my wish list</h4>
                </div>
                <div className='compare-icon d-flex align-items-center ml-xl'>
                    <Image
                        src={Compare}
                        alt="compare-icon"
                        width={20}
                        className='svg-icon'
                    />
                    <h4 className='mb-0 ml-sm'>Compare</h4>
                </div>
            </div>
        </div>
    )
}

export default ProductDetailContent
