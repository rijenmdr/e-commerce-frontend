import React, { useState } from 'react'
import { Col, Row } from 'react-bootstrap'
import { useQuery } from 'react-query'
import StarRatings from 'react-star-ratings'
import axios from '../../../../../../helper/axios'
import IndividualProductRating from '../../components/IndividualProductRating/IndividualProductRating'
import ProductReviewCard from '../../components/ProductReviewCard/ProductReviewCard';

import Loading from '../../../../components/Loading';
import NoData from '../../../../components/NoData';

const fetchProductRatings = async (parameter) => {
    console.log(parameter.queryKey)
    const [key, productId, limit] = parameter.queryKey
    console.log(key)
    const data = await axios.get(`/product/get-product-review/${productId}?limit=${limit}`)
    return data;
}

const ProductReviews = ({ productId, product }) => {
    const [limit, setLimit] = useState(5);

    const [ratingBar, setRatingBar] = useState({});
    const [productRatings, setProductRatings] = useState([])

    const { status } = useQuery(['productReview', productId, limit], fetchProductRatings, {
        enabled: !!productId,
        retry: false,
        refetchOnWindowFocus: false,
        onSuccess: (res) => {
            const result = {
                status: res.status + "-" + res.statusText,
                headers: res.headers,
                data: res.data,
            };
            setProductRatings(result.data.productReview);
            setRatingBar(result?.data?.totalReviews);
        }
    });

    return (
        <div className='product-reviews w-100'>
            {
                status === "loading" &&
                    <Loading/>
            }
             {
                status === "error" &&
                    <NoData/>
            }
            {
                status === "success" &&
                    productRatings.length !== 0 &&
                <>
                    <Row className='w-100 product-overall-review align-items-center'>
                        <Col lg="4">
                            <div className='rate-number d-flex align-items-center w-100'>
                                <h1 className='mb-0'>{product?.averageRating}</h1>
                                <h3 className='text-dark-light ml-sm mb-0'>/</h3>
                                <h3 className='text-dark-light ml-sm mb-0'>5</h3>
                            </div>
                            <StarRatings
                                rating={3.5}
                                starRatedColor="#FDBC15"
                                starDimension='25'
                                starSpacing='5'
                                numberOfStars={5}
                            />
                            <p className='text-light-dark mt-2'>
                                <u>{product?.reviews?.length} ratings</u>
                            </p>
                        </Col>
                        <Col lg="8">
                            {
                                ratingBar &&
                                Object.entries(ratingBar).map((rate, index) => (
                                    <>
                                        {rate[0] !== "_id" &&
                                            <IndividualProductRating
                                                rating={6 - index}
                                                percent={(rate[1] / product?.reviews?.length) * 100}
                                                number={rate[1]}
                                                key={rate[0]}
                                            />
                                        }
                                    </>
                                ))
                            }
                        </Col>
                    </Row>
                    <div className='divider'></div>
                    <div className='individual-reviews mt-5'>
                        <h3 className='mb-4'>Product Review</h3>
                        {
                            productRatings &&
                            productRatings.length !== 0 &&
                            productRatings.map(rating => (
                                <ProductReviewCard
                                    key={rating?._id}
                                    rating={rating}
                                />
                            ))
                        }
                        {
                            productRatings.length !== product?.reviews?.length &&
                            <h5
                                onClick={() => setLimit(prevVal => prevVal + 5)}
                                className="cursor-pointer ml-md mt-4 text-center"
                            >
                                View More
                            </h5>
                        }
                    </div>
                </>
            }
            {
                productRatings.length === 0 &&
                    <NoData/>
            }
        </div>
    )
}

export default ProductReviews
