
import React, { useEffect, useState } from 'react'
import { Col, Row } from 'react-bootstrap';
import Breadcrumbs from '../../container/Breadcrumb';
import ProductDetailContent from './container/ProductDetailContent/ProductDetailContent';
import ProductImageSection from './container/ProductImageSection/ProductImageSection';
import Tag from '../../components/Tag/Tag';

//scss
import './ProductDetail.scss';
import ProductDescription from './container/ProductDescription/ProductDescription';
import ProductReviews from './container/ProductReviews/ProductReviews';
import ProductQuestions from './container/ProductQuestions/ProductQuestions';
import RelatedProducts from './container/RelatedProducts/RelatedProducts';
import { useQuery } from 'react-query';
import { useParams } from 'react-router-dom';
import axios from '../../../../helper/axios';
import Loading from '../../components/Loading';
import NoData from '../../components/NoData';
import ReactHelmet from '../../../../components/ReactHelmet';

const fetchProductDetail = async (parameter) => {
    console.log(parameter.queryKey)
    const [key, id] = parameter.queryKey
    console.log(key)
    const data = await axios.get(`/product/get-product-detail/${id}`)
    return data;
}

const ProductDetail = () => {
    const { id } = useParams();
    const [product, setProduct] = useState(null);
    const [questionCount, setQuestionCount] = useState(0);
    const [relatedProducts, setRelatedProducts] = useState([])
    const [activeTab, setActiveTab] = useState(1);

    const { status } = useQuery(['productDetail', id], fetchProductDetail, {
        enabled: !!id,
        retry: false,
        refetchOnWindowFocus: false,
        onSuccess: (res) => {
            const result = {
                status: res.status + "-" + res.statusText,
                headers: res.headers,
                data: res.data,
            };
            setProduct(result.data.product);
            setRelatedProducts(result.data.relatedProducts);
            result.data.product !== 0 && setQuestionCount(result.data.product?.questionAnswer?.length)
        }
    });

    const changeTab = (tab) => {
        setActiveTab(tab);
    }

    useEffect(() => {
        window.scrollTo(0, 220);
        setActiveTab(1)
    }, [id])

    return (
        <div className='product-detail'>
             <ReactHelmet
                title={product?.name || "Product"}
            />
            {
                status === "loading" &&
                <Loading />
            }
            {
                status === "error" &&
                <NoData />
            }
            {
                status === "success" &&
                <>
                    <Breadcrumbs
                        title={product?.name}
                    />
                    <Row className='section-margin'>
                        <Col
                            className='product-detail-images d-flex flex-column'
                            lg="6"
                            md="12"
                        >
                            {
                                product?.productImages &&
                                <ProductImageSection
                                    images={product?.productImages}
                                />
                            }
                        </Col>
                        <Col className='product-content' lg="6">
                            <ProductDetailContent
                                product={product}
                            />
                            <div className='product-detail-tabs mt-5'>
                                <div className='tab-headings d-flex justify-content-between'>
                                    <h4
                                        onClick={() => changeTab(1)}
                                        className={`${activeTab === 1 ? 'active-tab' : ''}`}
                                    >
                                        Description
                                    </h4>
                                    <h4
                                        onClick={() => changeTab(2)}
                                        className={`${activeTab === 2 ? 'active-tab' : ''}`}
                                    >
                                        <span className='mr-sm'>Reviews</span>
                                        <Tag
                                            primary={true}
                                            value={(product?.reviews?.length)?.toString()}
                                        />
                                    </h4>
                                    <h4
                                        onClick={() => changeTab(3)}
                                        className={`${activeTab === 3 ? 'active-tab' : ''}`}
                                    >
                                        <span className='mr-sm'>Questions</span>
                                        <Tag
                                            primary={true}
                                            value={(questionCount)?.toString()}
                                        />
                                    </h4>
                                </div>
                                <div className='tab-content my-4 mx-2'>
                                    {
                                        activeTab === 1 &&
                                        <ProductDescription
                                            description={product?.description}
                                        />
                                    }
                                    {
                                        activeTab === 2 &&
                                        <ProductReviews
                                            product={product}
                                            productId={id}
                                        />
                                    }
                                    {
                                        activeTab === 3 &&
                                        <ProductQuestions
                                            product={product}
                                            productId={id}
                                            questionCount={questionCount}
                                            setQuestionCount={setQuestionCount}
                                        />
                                    }
                                </div>
                            </div>
                        </Col>
                    </Row>
                    {
                        relatedProducts && relatedProducts.length !== 0 &&
                        <div className='section-padding related-products mt-5'>
                            <RelatedProducts
                                products={relatedProducts}
                            />
                        </div>
                    }
                </>
            }
        </div>
    )
}

export default ProductDetail;
