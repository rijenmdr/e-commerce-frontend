import React, { useState } from 'react'
import { Button, Form, Spinner } from 'react-bootstrap'
import Tag from '../../../../components/Tag/Tag';
import TextAreaInput from '../../../../components/TextAreaInput/TextAreaInput';
import moment from 'moment';
import { Formik } from 'formik';
// import moment from 'moment';

import * as Yup from 'yup';
import { toast } from 'react-toastify';
import { useMutation } from 'react-query';
import axios from '../../../../../../helper/axios';
import { useSelector } from 'react-redux';
import Cookies from 'js-cookie';

const answerValidation = Yup.object().shape({
    answer: Yup.string()
        .required("*Answer is required")
})

const ProductQuestionAnswer = ({ questionAnswer, reply, product, refetch, setRefetching }) => {
    const [isReply, setIsReply] = useState(false);
    const [loading, setLoading] = useState(false);
    const [selectedQuestion, setSelectedQuestion] = useState(null);

    const id = useSelector(state => state.user?.currentUser?.id);

    const replyQuestion = (question) => {
        setIsReply(prevVal => !prevVal);
        setSelectedQuestion(question)
    }

    const mutation = useMutation(async (postData) => {
        console.log(postData)
        return await axios.post('/product/add-reply', postData, {
            headers: {
                'Authorization': 'Bearer ' + Cookies.get('token')
            }
        })
    }, {
        onSuccess: (res) => {
            refetch();
            toast.success("Question Answered added successfully");
            setLoading(false);
        },
        onError: () => {
            toast.error("Error while answering question");
            setLoading(false)
        }
    });

    console.log(id)

    return (
        <>
            <div className='p-2 m-2'>
                <div className='commenter-info d-flex align-items-center'>
                    <h1 className='mb-0'>Q</h1>
                    <div className='d-flex justify-content-between align-items-center w-100'>
                        <div className='commenter-info-text d-flex flex-column ml-md'>
                            <h5 className='mb-0'>{questionAnswer?.userId?.name}</h5>
                            <h6 className='text-dark-light mb-0'>
                                {moment(questionAnswer?.createdAt).startOf('seconds').fromNow()}
                            </h6>
                        </div>

                    </div>

                </div>
                <div className='commenter-comment mt-4 pl-lg'>
                    <h5>
                        {questionAnswer?.question}
                    </h5>
                </div>
                {!questionAnswer?.reply &&  id === product?.merchantId?._id &&
                    <>
                        <h5
                            onClick={() => replyQuestion(questionAnswer)}
                            className='text-secondary cursor-pointer pl-lg mb-0'
                        >
                            Reply
                        </h5>
                        {
                            isReply &&
                            <Formik
                                initialValues={{ answer: "" }}
                                validationSchema={answerValidation}
                                onSubmit={(values, { setSubmitting, resetForm }) => {
                                    setLoading(true);
                                    setRefetching(true);
                                    const postData = {
                                        reply: values.answer,
                                        questionId: selectedQuestion._id
                                    }
                                    mutation.mutate(postData);
                                    resetForm();
                                }}
                            >
                                {({
                                    values,
                                    errors,
                                    touched,
                                    handleChange,
                                    handleBlur,
                                    handleSubmit,
                                }
                                ) => (
                                    <Form onSubmit={handleSubmit}>
                                        <div className='ml-lg'>
                                            <TextAreaInput
                                                name="answer"
                                                row={5}
                                                value={values.answer}
                                                handleChange={handleChange}
                                                handleBlur={handleBlur}
                                                placeholder='Answer this question'
                                                touched={touched.question}
                                                errors={errors.question}
                                            />
                                            <Button
                                                size="sm"
                                                type="submit"
                                                disabled={loading}
                                            >
                                                {
                                                    loading ?
                                                        <Spinner
                                                            variant='light'
                                                            animation='border'
                                                            size='sm'
                                                        /> :
                                                        'Reply'
                                                }
                                            </Button>
                                        </div>
                                    </Form>
                                )}
                            </Formik>
                        }
                    </>

                }
            </div>
            {questionAnswer?.reply &&
                questionAnswer?.reply !== "" &&
                <div className='p-2 overflow-none ml-xl'>
                    <div className='commenter-info d-flex align-items-center'>
                        <h1 className='mb-0'>A</h1>
                        <div className='d-flex justify-content-between align-items-center w-100'>
                            <div className='commenter-info-text d-flex flex-column ml-md'>
                                <h5 className='mb-0'>{product?.merchantId?.name}
                                    <Tag
                                        value="Merchant"
                                        primary={true}
                                    />
                                </h5>
                                <h6 className='text-dark-light mb-0'>
                                    {moment(questionAnswer?.updatedAt).startOf('seconds').fromNow()}
                                </h6>
                            </div>

                        </div>

                    </div>
                    <div className='commenter-comment mt-4 pl-xl'>
                        <h5>
                            {questionAnswer?.reply}
                        </h5>
                    </div>
                </div>
            }
            <div className='divider p-1'></div>
        </>
    )
}

export default ProductQuestionAnswer
