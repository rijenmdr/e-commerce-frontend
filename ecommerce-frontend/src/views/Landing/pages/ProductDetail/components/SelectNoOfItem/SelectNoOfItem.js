import React from 'react'
import { Form, InputGroup } from 'react-bootstrap'

const SelectNoOfItem = ({ onStock, soldIn, value, onChange, disabled }) => {
    console.log(onStock)
    return (
        <InputGroup className="d-flex select-no-item align-items-center" size="sm">

            <Form.Select 
                size="sm" 
                className="select-no border-0 bg-transparent box-shadow-none mr-sm"
                value={value}
                onChange={onChange}
                disabled={disabled}
            >
                {
                    Array.from({length : onStock} ,(stock, index)=>(
                        <option
                            key={index}
                            value={index + 1}
                        >
                            {index + 1}
                        </option>
                    ))
                }
            </Form.Select>

            <div className="border-vertical mr-md" />
            <InputGroup.Text
                className='border-0 bg-transparent'
            >
                <h5 className='mb-0'>{soldIn}</h5>
            </InputGroup.Text>
        </InputGroup>
    )
}

export default SelectNoOfItem
