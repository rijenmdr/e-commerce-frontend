import React, { useEffect, useState } from 'react';
import { Button, Modal, Image } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import CartProductCard from '../../components/ProductCard/CartProductCard';
import { useNavigate } from 'react-router-dom';

//svg
import EmptyCart from '../../../../assets/images/components/emptyCart.svg';

const CartModal = ({
    modal,
    closeModal
}) => {
    const navigate = useNavigate();

    const cart = useSelector(state => state.user.cart);

    const [amount, setAmount] = useState(0);

    const calculateAmount = () => {
        let am = 0;
        cart && cart.map((a, b) => {
            am = am + a?.actualPrice * a?.numberOfItem;
            return am;
        });
        setAmount(am);
    }

    useEffect(() => {
        if (cart && cart.length !== 0) {
            calculateAmount();
        }
    }, [cart]);

    const goToProductsPage = () => {
        closeModal();
        navigate('/products');
    }

    const checkout = () => {
        closeModal();
        navigate('/checkout')
    }

    return (
        <Modal
            style={{
                height: "100vh"
            }}
            show={modal}
            onHide={closeModal}
        >
            <Modal.Header closeButton>
                <Modal.Title id="example-custom-modal-styling-title">
                    Shopping Cart
                </Modal.Title>
            </Modal.Header>
            <Modal.Body
                className='overflow-y-auto d-flex flex-column'
            >
                {
                    cart &&
                        cart.length !== 0 ?
                        cart.map(cartItem => (
                            <CartProductCard
                                key={cartItem?.id}
                                cart={cartItem}
                            />
                        )) :
                        <div className='d-flex justify-content-center flex-column'>
                            <h4 className='text-center'>Cart is empty</h4>
                            <Image
                                src={EmptyCart}
                                style={{
                                    height: "50vh",
                                    width: "100%"
                                }}
                            />
                            <h5
                                onClick={goToProductsPage}
                                className='text-center text-secondary cursor-pointer'
                            >
                                Add some product to cart.
                            </h5>
                        </div>
                }
            </Modal.Body>
            <Modal.Footer
                className='d-flex justify-content-between'
            >
                <div className=''>
                    <h5
                        className='mb-0'
                    >
                        Total
                    </h5>
                    <h3 className='mb-0'>NRp. {amount}</h3>
                </div>
                <Button
                    onClick={checkout}
                    size='sm'
                    type="button"
                    disabled={cart?.length === 0}
                >
                    Go to Checkout
                </Button>
            </Modal.Footer>
        </Modal>
    );
};

export default CartModal;
