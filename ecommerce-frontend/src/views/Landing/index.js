import React, { useState } from 'react';
import Footer from './container/Footer';
import Header from './container/Header';
import Nav from './container/Nav';
import LandingRoutes from './LandingRoutes';

import '../../assets/scss/component.scss';
import { useQuery } from 'react-query';
import Cookies from 'js-cookie';
import { getCurrentUser } from '../../helper/utils';
import { useDispatch } from 'react-redux';
import { loginSuccess } from '../../redux/actions';
import { errorResponse } from '../../helper/errorResponse';
import CartModal from './container/Cart';
import { initialUser, userError } from '../../redux/user/actions';

const LandingLayout = () => {
    const [showCart, setShowCart] = useState(false);

    const token = Cookies.get("token");
    const dispatch = useDispatch();

    
    useQuery('currentUser', () => getCurrentUser(dispatch), {
        enabled: !!token,
        retry: false,
        refetchOnWindowFocus: false,
        onSuccess: (res)=>{
            const result = {
                status: res?.status,
                data: res?.data,
              };
            dispatch(loginSuccess(result.data?.currentUser));
        },
        onError: (res) => {
            errorResponse(res);
            dispatch(userError());
        }
    });

    return (
        <div>
            <Header
                setShowCart={setShowCart}
            />
            <Nav/>
            <LandingRoutes/>
            <Footer/>

            <CartModal
                modal={showCart}
                closeModal={()=>setShowCart(false)}
            />
        </div>
    )
}

export default LandingLayout;
