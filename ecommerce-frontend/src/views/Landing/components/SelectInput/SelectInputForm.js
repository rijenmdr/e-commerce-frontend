import React from 'react';
import { Form } from 'react-bootstrap';
// import PropTypes from 'prop-types';

const SelectInputForm = (
    {
        selectOptions,
        name,
        label,
        value,
        handleChange,
        handleBlur,
        handleFocus,
        disabled,
        touched,
        errors
    }) => {
    return (
        <Form.Group className="mb-3 select-input" controlId="exampleForm.ControlInput1">
            <Form.Label>{label}</Form.Label>
            <Form.Select
                size="xl"
                className=""
                name={name}
                value={value}
                disabled={disabled || false}
                onChange={handleChange}
                onBlur={handleBlur}
                onFocus={handleFocus}
                className={`input-box border-0 bg-transparent box-shadow-none cursor-pointer ${touched && errors ? "input-error" : null}`}
            >
                <option value="">Select {label}</option>
                {selectOptions.map(options => (
                    <option key={options.id} value={options.value}>
                        {options.label}
                    </option>
                ))}
            </Form.Select>
            {touched && errors ? (
                <div className="error-message">{errors}</div>
            ) : null}
        </Form.Group>
    )
}

// SelectInput.propTypes = {
//     selectOptions: PropTypes.array.isRequired,
//     value: PropTypes.any,
//     setValue: PropTypes.func,
// }

export default SelectInputForm
