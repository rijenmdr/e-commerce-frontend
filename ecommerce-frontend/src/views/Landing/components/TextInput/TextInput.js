import React from 'react';
import { Form, InputGroup } from 'react-bootstrap';
import PropTypes from 'prop-types';

import { FaEye, FaEyeSlash } from 'react-icons/fa'

const TextInput = ({
    name,
    label,
    type,
    value,
    min,
    maxLength,
    handleChange,
    handleBlur,
    handleFocus,
    placeholder,
    disabled,
    touched,
    errors,
    setVisible,
    autocomplete,
    readOnly
}) => {
    return (
        <Form.Group className="mb-3 text-input position-relative" controlId="exampleForm.ControlInput1">
            <Form.Label>{label}</Form.Label>
            <Form.Control
                name={name}
                value={value}
                type={type}
                disabled={disabled || false}
                placeholder={placeholder}
                onChange={handleChange}
                onBlur={handleBlur}
                onFocus={handleFocus}
                className={touched && errors ? "input-error" : null}
                min={min}
                maxLength={maxLength}
                autoComplete= {autocomplete}
                readOnly={readOnly}
            />
            {
                (name === "password" || name === "confirmPassword") &&
                <InputGroup.Text
                    style={{
                        right: "3px",
                        top: "39px"
                    }}
                    onClick={() => setVisible(prevVal => !prevVal)}
                    className="border-0 bg-transparent cursor-pointer position-absolute"
                >
                    {
                        type === "password" ? <FaEyeSlash /> : <FaEye />
                    }
                </InputGroup.Text>
            }

            {touched && errors ? (
                <div className="error-message">{errors}</div>
            ) : null}
        </Form.Group>
    )
}

TextInput.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    value: PropTypes.any,
    placeholder: PropTypes.string.isRequired,
    disabled: PropTypes.bool,
    handleChange: PropTypes.func.isRequired,
    handleBlur: PropTypes.func,
    touched: PropTypes.any,
    errors: PropTypes.string
}

export default TextInput
