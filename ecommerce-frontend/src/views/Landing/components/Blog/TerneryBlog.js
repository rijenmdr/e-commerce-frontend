import React from 'react';
import { Image, Row } from 'react-bootstrap';
import moment from 'moment';
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';

const TerneryBlog = ({ blog }) => {
    const navigate = useNavigate();

    const navigateToBlogDetail = (id) => {
        navigate(`/blogs/${id}`)
    }

    return (
        <Row 
            onClick={()=>navigateToBlogDetail(blog?._id)}
            className='last-row-blog cursor-pointer mb-5'
        >
            <div className="d-flex justify-content-between">
                <div className="w-50 blog-desc">
                    <h5 className="desc">
                        {blog?.title}
                    </h5>
                    <div className="d-flex mt-2">
                        <h6>{blog?.authorId?.name}</h6>
                        <h6 className="ml-xl">{moment(blog?.createdAt).format('ll')}</h6>
                    </div>
                </div>
                <Image
                    width={100}
                    src={blog?.previewImg}
                    rounded 
                />
            </div>
        </Row>

    )
}

TerneryBlog.propTypes = {
    blog: PropTypes.object.isRequired
}

export default TerneryBlog
