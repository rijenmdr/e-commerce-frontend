import React from 'react';
import { Card, CardImg, Image } from 'react-bootstrap';
import PropTypes from 'prop-types';
import moment from 'moment';

import Tag from '../Tag/Tag';
import { useNavigate } from 'react-router-dom';

const PrimaryBlog = ({ blog }) => {
    const navigate = useNavigate(); 
    return (
        <Card 
            onClick={()=>navigate(`/blogs/${blog?._id}`)}
            className="cursor-pointer position-relative primary-blog-card"
        >
            <CardImg
                className="blog1-img"
                src={blog?.previewImg} />
            <div className="position-absolute blog-tag">
                <Tag
                    value={blog?.categoryId?.name}
                    primary={true}
                />
            </div>
            <div className="position-absolute blog-details">
                <h3 className="text-light blog-title">
                    {blog?.title}
                </h3>
                <div className="d-flex align-items-center blog-add">
                    <Image
                        className="avatar"
                        src={blog?.authorId?.profileImage ? blog?.authorId?.profileImage : 'https://www.nicepng.com/png/detail/136-1366211_group-of-10-guys-login-user-icon-png.png'}
                        width={40}
                        height={40}
                        roundedCircle
                    />
                    <h6 className="text-light ml-xl">{blog?.authorId?.name}</h6>
                    <h6 className="ml-xl text-light">{moment(blog?.createdAt).format('ll')}</h6>
                </div>
            </div>
        </Card>
    )
}

PrimaryBlog.propTypes = {
    blog: PropTypes.object.isRequired
}

export default PrimaryBlog
