import React, { useState } from 'react';
import { Card, Col, Dropdown, Image, Row, Spinner } from 'react-bootstrap';

import Heart from '../../../../assets/images/icons/ic-actions-heart-red.svg';
import Compare from '../../../../assets/images/icons/ic-layout-picture-red-right.svg';
import Close from '../../../../assets/images/icons/ic-actions-close-simple.svg';
import ProductExtraInfo from '../../pages/ProductDetail/components/ProductExtraInfo/ProductExtraInfo';
import StarRatings from 'react-star-ratings';
import SelectNoOfItem from '../../pages/ProductDetail/components/SelectNoOfItem/SelectNoOfItem';
import { useMutation } from 'react-query';
import { addProductToFavourite } from '../../../../helper/utils';
import { toast } from 'react-toastify';
import { errorResponse } from '../../../../helper/errorResponse';
import { useDispatch } from 'react-redux';
import { updateCart } from '../../../../redux/actions';
import axios from '../../../../helper/axios';
import Cookies from 'js-cookie';

const CartProductCard = ({ cart }) => {
    const [favLoading, setFavLoading] = useState(false);
    const [removeLoading, setRemoveLoading] = useState(false);
    const [disable, setDisable] = useState(false);

    const dispatch = useDispatch();

    const favMutation = useMutation(async (productId) => {
        setFavLoading(true);

        await addProductToFavourite(productId)
    }, {
        onSuccess: (res) => {
            toast.success("Product added to favourite successfully");
            setFavLoading(false);
        },
        onError: (res) => {
            errorResponse(res)
            setFavLoading(false)
        }
    });

    const removeMutation = useMutation(async (productId) => {
        setRemoveLoading(true);

        return await axios.post('/product/remove-from-cart', {
            productId
        }, {
            headers: {
                'Authorization': 'Bearer ' + Cookies.get('token')
            }
        })
    }, {
        onSuccess: (res) => {
            toast.success("Product removed from cart successfully");
            dispatch(updateCart(res.data.cart));
            setRemoveLoading(false);
        },
        onError: (res) => {
            errorResponse(res)
            setRemoveLoading(false)
        }
    });

    const updateMutation = useMutation(async (postData) => {
        console.log(postData)
        return await axios.post('/product/update-cart', postData, {
            headers: {
                'Authorization': 'Bearer ' + Cookies.get('token')
            }
        })
    }, {
        onSuccess: (res) => {
            toast.success("Number of items in cart updated successfully");
            dispatch(updateCart(res.data.cart));
            setDisable(false);
        },
        onError: (res) => {
            errorResponse(res)
            setDisable(false)
        }
    });

    const handleCartNumberChange = (e, productId) => {
        const numberOfItem = e.target.value;
        const postData = {
            productId,
            numberOfItem
        }
        setDisable(true);
        updateMutation.mutate(postData);
    }

    return (
        <Row className='py-4'>
            <Col xl="4" lg="4" md="4" sm="4">
                <Card
                >
                    <Image
                        style={{
                            backgroundSize: "cover",
                            maxHeight: "150px"
                        }}
                        src={cart?.previewImg}
                    />
                </Card>
                <div className='mt-2'>
                    <div
                        onClick={() => favMutation.mutate(cart?.id)}
                        className='d-flex cursor-pointer'
                    >
                        {
                            favLoading ?
                                <Spinner
                                    variant='primary'
                                    animation='border'
                                    size='sm'
                                /> :
                                <Image
                                    src={Heart}
                                    alt="heart-icon"
                                    width={15}
                                    className='svg-icon'
                                />
                        }
                        <h5 className='mb-0 ml-sm'>Wishlist</h5>
                    </div>
                    <div className='d-flex mt-1 cursor-pointer'>
                        <Image
                            src={Compare}
                            alt="heart-icon"
                            width={15}
                            className='svg-icon'
                        />
                        <h5 className='mb-0 ml-sm'>Compare</h5>
                    </div>
                    <div
                        onClick={() => removeMutation.mutate(cart?.id)}
                        className='d-flex mt-1 cursor-pointer'
                    >
                        {
                            removeLoading ?
                                <Spinner
                                    variant='primary'
                                    animation='border'
                                    size='sm'
                                /> :
                                <Image
                                    src={Close}
                                    alt="heart-icon"
                                    width={15}
                                    className='svg-icon'
                                />
                        }
                        <h5 className='mb-0 ml-sm'>Remove</h5>
                    </div>
                </div>
            </Col>
            <Col xl="8" lg="8" md="8" sm="8">
                <h3>{cart?.name}</h3>
                <div className='d-flex flex-column mb-1'>
                    <ProductExtraInfo
                        title="On Stock"
                        value={cart?.onStock}
                        margin="mb-0"
                    />
                    <ProductExtraInfo
                        title="Delivery In"
                        value={`${cart?.numberForDelivery} ${cart?.timeForDelivery}`}
                        margin="mb-0"
                    />
                     <ProductExtraInfo
                        title="Delivery Cost"
                        value={cart?.shippingCost !== 0 ? `Nrp. ${cart?.shippingCost}` : 'Free'}
                        margin="mb-0"
                    />
                </div>
                <StarRatings
                    rating={Number(cart?.averageRating)}
                    starRatedColor="#FDBC15"
                    starDimension='20'
                    starSpacing='5'
                    numberOfStars={5}
                />
                <div className='d-flex justify-content-between align-items-center mt-2'>
                    <div className='product-prices'>
                        <h3 className='mb-0 text-secondary'>NRp. {cart?.actualPrice}</h3>
                    </div>
                    <div>
                        <SelectNoOfItem
                            onStock={cart?.onStock}
                            value={cart?.numberOfItem}
                            disabled={disable}
                            onChange={(e) => handleCartNumberChange(e, cart?.id)}
                            soldIn={cart?.soldIn}
                        />
                    </div>
                </div>
            </Col>
            <Dropdown.Divider className='mt-4' />
        </Row>
    );
};

export default CartProductCard;
