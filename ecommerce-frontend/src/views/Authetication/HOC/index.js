import React from 'react';
import { Col, Image, Row } from 'react-bootstrap';

import Logo from '../../../assets/images/logo.svg';
import Authentication from '../../../assets/images/components/authentication.svg';
import { useNavigate } from 'react-router-dom';

const HOC = ({ children }) => {
    const navigate = useNavigate();
    return (
        <div className='authentication'>
            <Row className="w-100 d-flex">
                <Col className="p-2" xl="7" lg="6" md="5">
                    <Image
                        className="cursor-pointer mt-md ml-xl"
                        src={Logo}
                        onClick={() => navigate('/')}
                    />
                    <div className='ml-0 auth-img-div'>
                        <Image
                            className="auth-image"
                            src={Authentication}
                            style={{
                                height: "100vh",
                            }}
                        />
                    </div>
                </Col>
                <Col className='p-5' xl="5" lg="6" md="7">
                    {children}
                </Col>
            </Row>
        </div>
    )
}

export default HOC
