import { Formik } from 'formik';
import moment from 'moment';
import React from 'react'
import { Button, Col, Form, Row } from 'react-bootstrap';
import * as Yup from 'yup'

import SelectInputForm from '../../../../../Landing/components/SelectInput/SelectInputForm';
import TextInput from '../../../../../Landing/components/TextInput/TextInput';

const stepOneValidationSchema = Yup.object().shape({
    firstName: Yup.string().required("First Name is required"),
    lastName: Yup.string().required("Last Name is required"),
    gender: Yup.string().required("Gender is required"),
    dob: Yup.string().required("DOB is required").test(
        "DOB",
        "User must be older than 16 years old",
        value => {
            return moment().diff(moment(value), 'years') >= 16
        }
    ),
    street: Yup.string().required("Street is required"),
    city: Yup.string().required("City is required"),
    country: Yup.string().required("Country is required")
});

const BasicInformation = ({ data, handleNextStep }) => {
    const selectOptions = [
        {
            id: 1,
            label: "Male",
            value: "m",
        },
        {
            id: 2,
            label: "Female",
            value: "f",
        },
        {
            id: 3,
            label: "Other",
            value: "o",
        },
    ]
    return (
        <Formik
            initialValues={data}
            validationSchema={stepOneValidationSchema}
            onSubmit={(values, { setSubmitting, resetForm }) => {
                console.log(values)
                handleNextStep(values)
            }}
        >
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
            }
            ) => (
                <Form onSubmit={handleSubmit}>
                    <Row className='mt-4'>
                        <Col xl="6">
                            <TextInput
                                name="firstName"
                                label="First Name"
                                type="text"
                                value={values.firstName}
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                                placeholder={"Enter your First Name"}
                                // disabled={isLoading}
                                touched={touched.firstName}
                                errors={errors.firstName}
                            />
                        </Col>

                        <Col xl="6">
                            <TextInput
                                name="lastName"
                                label="Last Name"
                                type="text"
                                value={values.lastName}
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                                placeholder={"Enter your Last Name"}
                                // disabled={isLoading}
                                touched={touched.lastName}
                                errors={errors.lastName}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col lg="6">
                            <SelectInputForm
                                selectOptions={selectOptions}
                                name="gender"
                                label="Gender"
                                value={values.gender}
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                                // disabled={isLoading}
                                touched={touched.gender}
                                errors={errors.gender}

                            />
                        </Col>
                        <Col xl="6">
                            <TextInput
                                name="dob"
                                label="Date of Birth"
                                type="date"
                                value={values.dob}
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                                placeholder={"Enter your Date of Birth"}
                                // disabled={isLoading}
                                touched={touched.dob}
                                errors={errors.dob}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col xl="6">
                            <TextInput
                                name="street"
                                label="Street"
                                type="text"
                                value={values.street}
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                                placeholder={"Enter your Street"}
                                // disabled={isLoading}
                                touched={touched.street}
                                errors={errors.street}
                            />
                        </Col>

                        <Col xl="6">
                            <TextInput
                                name="city"
                                label="City"
                                type="text"
                                value={values.city}
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                                placeholder={"Enter your City"}
                                // disabled={isLoading}
                                touched={touched.city}
                                errors={errors.city}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <TextInput
                            name="country"
                            label="Country"
                            type="text"
                            value={values.country}
                            handleChange={handleChange}
                            handleBlur={handleBlur}
                            placeholder={"Enter your Country"}
                            // disabled={isLoading}
                            touched={touched.country}
                            errors={errors.country}
                        />
                    </Row>
                    <div className="mt-2">
                        <Button
                            type="submit"
                            size='sm'
                            className='w-100'
                        // disabled={loading}
                        >
                            Continue
                        </Button>
                    </div>
                </Form>
            )}
        </Formik>
    )
}

export default BasicInformation
