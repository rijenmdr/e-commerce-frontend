import React, { useState } from 'react'
import BasicInformation from './BasicInformation';
import LoginInformation from './LoginInformation';

const UserRegister = ({ data, setData, mutation, loading }) => {
    const [stage, setStage] = useState(1);

    const handleNextStep = (newData, final = false) => {
        console.log(newData)
        setData((prev) => ({ ...prev, ...newData }));

        if (final) {
            return mutation.mutate()
        }

        setStage((prev) => prev + 1);
    };

    const handlePrevStep = (newData) => {
        console.log(newData)
        setData((prev) => ({ ...prev, ...newData }));
        setStage((prev) => prev - 1);
    };

    return (
        <>
            <h2>
                {
                    stage === 1 ?
                        'Basic Information' : 'Login Information'
                }
            </h2>
            {
                stage === 1 &&
                <BasicInformation
                    data={data}
                    handleNextStep={handleNextStep}
                />
            }
            {
                stage === 2 &&
                <LoginInformation
                    data={data}
                    loading={loading}
                    handleNextStep={handleNextStep}
                    handlePrevStep={handlePrevStep}
                />
            }
        </>
    )
}

export default UserRegister
