import { Formik } from 'formik';
import React, { useState } from 'react'
import { Button, Col, Form, Row, Spinner } from 'react-bootstrap';
import * as Yup from 'yup'
import { passwordValidation } from '../../../../../../helper/validation';

import TextInput from '../../../../../Landing/components/TextInput/TextInput';

const stepOneValidationSchema = Yup.object().shape({
    email: Yup.string()
        .required("Email is required")
        .email("*Email Address is invalid"),
    password: Yup.string()
        .required("Password is required")
        .matches(passwordValidation, "Password must be minimum of 6 characters with number and letters"),
    confirmPassword: Yup.string()
        .oneOf([Yup.ref('password'), null], '*Passwords doesnot match')
});

const LoginInformation = ({ data, handleNextStep, handlePrevStep, loading }) => {
    const [passwordVisible, setPasswordVisible] = useState(false);
    return (
        <Formik
            initialValues={data}
            validationSchema={stepOneValidationSchema}
            onSubmit={(values) => {
                handleNextStep(values, true);
            }}
        >
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
            }
            ) => (
                <Form onSubmit={handleSubmit}>
                    <Row className='mt-4'>
                        <Col xl="12">
                            <TextInput
                                name="email"
                                label="Email"
                                type="text"
                                value={values.email}
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                                placeholder={"Enter your Email"}
                                disabled={loading}
                                touched={touched.email}
                                errors={errors.email}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col xl="6">
                            <TextInput
                                name="password"
                                label="Password"
                                type={passwordVisible ? 'text' : `password`}
                                setVisible={setPasswordVisible}
                                value={values.password}
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                                placeholder={"Enter Password"}
                                disabled={loading}
                                touched={touched.password}
                                errors={errors.password}
                            />
                        </Col>

                        <Col xl="6">
                            <TextInput
                                name="confirmPassword"
                                label="Confirm Password"
                                type={passwordVisible ? 'text' : `password`}
                                setVisible={setPasswordVisible}
                                value={values.confirmPassword}
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                                placeholder={"Enter Confirm Password"}
                                disabled={loading}
                                touched={touched.confirmPassword}
                                errors={errors.confirmPassword}
                            />
                        </Col>
                    </Row>
                    <div className="mt-2 d-flex justify-content-between">
                        <Button
                            type="button"
                            onClick={() => handlePrevStep(values)}
                            size='sm'
                            className='w-40 w-100 btn-secondary'
                            disabled={loading}
                        >
                            Back
                        </Button>
                        <Button
                            type="submit"
                            size='sm'
                            className='w-100'
                            disabled={loading}
                        >
                            {
                                loading ?
                                    <Spinner
                                        variant='light'
                                        animation='border'
                                        size='sm'
                                    /> :
                                    'Register'
                            }
                        </Button>
                    </div>
                </Form>
            )}
        </Formik>
    )
}

export default LoginInformation
