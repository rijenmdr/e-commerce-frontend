import React, { useEffect, useState } from 'react'
import { Formik } from 'formik';
import { Button, Col, Form, Row, Spinner } from 'react-bootstrap';
import TextInput from '../../../../../Landing/components/TextInput/TextInput';
import { toast } from 'react-toastify';

import { auth } from '../../../../../../firebase';
import { signInWithPhoneNumber, RecaptchaVerifier } from "firebase/auth";

import * as Yup from 'yup';

const OTPValidation = Yup.object().shape({
    otp: Yup.string()
        .required("*OTP is required")
})

const OTP = ({ loading, setLoading, setPhase, mobile }) => {
    const [counter, setCounter] = useState(30);

    const validateOTP = (otp) => {
        const code = otp;
        window.confirmationResult.confirm(code).then((result) => {
            // User signed in successfully.
            const user = result.user;
            console.log(user);

            toast.success("OTP has been verified successfully");
            setLoading(false);

            setPhase("user-register");
        }).catch((error) => {
            toast.success("Error while verifying OTP");
            setLoading(false);
        });
    }

    useEffect(() => {
        const timer = counter &&
            setInterval(() => {
                setCounter(counter - 1)
            }, 1000);

        return () => clearInterval(timer);
    }, [counter]);

    const configureCaptcha = () => {
        window.recaptchaVerifier = new RecaptchaVerifier('sign-in-button', {
            'size': 'invisible',
            'callback': (response) => {
                // reCAPTCHA solved, allow signInWithPhoneNumber.
                onSignInSubmit();
            }
        }, auth);
    }

    const onSignInSubmit = async () => {
        setLoading(true);
        configureCaptcha();
        const phoneNumber = "+977" + mobile;

        const appVerifier = window.recaptchaVerifier;

        signInWithPhoneNumber(auth, phoneNumber, appVerifier)
            .then((confirmationResult) => {
                window.confirmationResult = confirmationResult;
                setLoading(false);
                setCounter(30);
                toast.success("OTP has been resent to your mobile number successfully");
            }).catch((error) => {

                if (error.message !== "reCAPTCHA has already been rendered in this element") {
                    toast.error("Error while sending OTP");
                }
                setLoading(false);
            });
    }


    return (
        <>
            <h2>Verify your Mobile Number</h2>
            <Formik
                initialValues={{ otp: "" }}
                validationSchema={OTPValidation}
                onSubmit={(values, { setSubmitting, resetForm }) => {
                    setLoading(true);
                    validateOTP(values.otp);
                }}
            >
                {({
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                }
                ) => (
                    <Form onSubmit={handleSubmit}>
                        <div id="sign-in-button"></div>
                        <Row className='mt-4'>
                            <Col xl="12">
                                <TextInput
                                    name="otp"
                                    label="OTP"
                                    type="text"
                                    value={values.otp}
                                    handleChange={handleChange}
                                    handleBlur={handleBlur}
                                    placeholder={"Please enter OTP"}
                                    disabled={loading}
                                    touched={touched.otp}
                                    errors={errors.otp}
                                />
                            </Col>
                        </Row>
                        <div className="mt-2 d-flex justify-content-between">
                            <Button
                                onClick={onSignInSubmit}
                                type="submit"
                                size='sm'
                                className='btn-secondary w-40 mr-sm'
                                disabled={loading || counter !== 0}
                            >
                                {`Resend OTP ${counter !== 0 ? `at ${counter}s` : ''}`}
                            </Button>
                            <Button
                                type="submit"
                                size='sm'
                                className='w-40'
                                disabled={loading}
                            >
                                {
                                    loading ?
                                        <Spinner
                                            variant='light'
                                            animation='border'
                                            size='sm'
                                        /> :
                                        'Verify OTP'
                                }
                            </Button>
                        </div>
                    </Form>
                )}
            </Formik>
        </>
    )
}

export default OTP
