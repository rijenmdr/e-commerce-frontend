import React from 'react'
import { Formik } from 'formik';
import { Button, Col, Form, Row, Spinner } from 'react-bootstrap';
import TextInput from '../../../../../Landing/components/TextInput/TextInput';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify'

import { auth } from '../../../../../../firebase';
import { signInWithPhoneNumber, RecaptchaVerifier } from "firebase/auth";

import { mobileNoValidation } from '../../../../../../helper/validation';
import * as Yup from 'yup';

import axiosInstances from '../../../../../../helper/axios';
import { useMutation } from 'react-query';
import { errorResponse } from '../../../../../../helper/errorResponse';
import GoogleLogin from 'react-google-login';
import Cookies from 'js-cookie';


const mobileRegisterValidation = Yup.object().shape({
    mobile: Yup.string()
        .required("*Mobile Number is required")
        .matches(mobileNoValidation, "Invalid Mobile Number")
})

const MobileRegister = ({ loading, setLoading, setOTP, setMobile, type }) => {
    const navigate = useNavigate();

    const CLIENT_ID = process.env.REACT_APP_GOOGLE_CLIENT_ID

    const mutation = useMutation(async (mobile) => {
        return await axiosInstances.post('/user/mobile-verify', {
            mobileNo: mobile
        })
    }, {
        onSuccess: (res) => {
            const result = {
                status: res.status + "-" + res.statusText,
                headers: res.headers,
                data: res.data,
            };
            if (type === "register") {
                if (result.data.state === "new") {
                    onSignInSubmit(res.data.number);
                } else {
                    toast.error("Mobile Number Already Registered");
                    setLoading(false);
                }
            } else {
                if (result.data.state === "new") {
                    toast.error("Mobile Number doesnot exists");
                    setLoading(false);
                } else {
                    onSignInSubmit(res.data.number);
                }
            }
        },
        onError: (res) => {
            errorResponse(res)
            setLoading(false)
        }
    });

    const configureCaptcha = () => {
        window.recaptchaVerifier = new RecaptchaVerifier('sign-in-button', {
            'size': 'invisible',
            'callback': (response) => {
                // reCAPTCHA solved, allow signInWithPhoneNumber.
                onSignInSubmit();
            }
        }, auth);
    }

    const handleRegister = async (response) => {
        setLoading(true);
        try {
            const register = await axiosInstances.post('/user/google-login', {
                token: response?.tokenId
            });
            toast.success(`Welcome ${register.data?.userDetail?.name}`);
            Cookies.set("token", register.data?.userDetail?.access_token);
            navigate('/');
            setLoading(false);
        } catch (err) {
            errorResponse(err);
            setLoading(false);
        }
    }

    const onSignInSubmit = async (mobile) => {
        configureCaptcha();
        const phoneNumber = "+977" + mobile;

        const appVerifier = window.recaptchaVerifier;

        signInWithPhoneNumber(auth, phoneNumber, appVerifier)
            .then((confirmationResult) => {
                window.confirmationResult = confirmationResult;
                setLoading(false);
                toast.success("OTP has been sent to your mobile number successfully");
                setMobile(mobile);
                setOTP(true);

            }).catch((error) => {

                if (error.message !== "reCAPTCHA has already been rendered in this element") {
                    toast.error("Error while sending OTP");
                }
                setLoading(false);
            });
    }
    return (
        <>
            <h2>{type === "register" ?
                'Create Your Account' : 'Change Password'}</h2>
            <h5 className='mr-0 mt-4'>
                Already have an account? <span onClick={() => navigate('/user/login')} className='text-secondary cursor-pointer'>
                    Login
                </span>
            </h5>
            <Formik
                initialValues={{ mobile: "" }}
                validationSchema={mobileRegisterValidation}
                onSubmit={(values, { setSubmitting, resetForm }) => {
                    setLoading(true);
                    mutation.mutate(values.mobile);
                }}
            >
                {({
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                }
                ) => (
                    <Form onSubmit={handleSubmit}>
                        <div id="sign-in-button"></div>
                        <Row className='mt-4'>
                            <Col xl="12">
                                <TextInput
                                    name="mobile"
                                    label="Mobile Number"
                                    type="text"
                                    value={values.mobile}
                                    maxLength={10}
                                    handleChange={handleChange}
                                    handleBlur={handleBlur}
                                    placeholder={"Please enter your mobile number"}
                                    disabled={loading}
                                    touched={touched.mobile}
                                    errors={errors.mobile}
                                />
                            </Col>
                        </Row>
                        <div className="mt-2">
                            <Button
                                type="submit"
                                size='sm'
                                className='w-100'
                                disabled={loading}
                            >
                                {
                                    loading ?
                                        <Spinner
                                            variant='light'
                                            animation='border'
                                            size='sm'
                                        /> :
                                        'Send OTP'
                                }
                            </Button>
                        </div>
                        {type === "register" &&
                            <>
                                <div class="divider-line-x mt-2"><span>Or Register with</span></div>

                                <div className='mt-4'>
                                    <GoogleLogin
                                        className='w-100 btn btn-secondary d-flex justify-content-center'
                                        clientId={CLIENT_ID}
                                        buttonText="Register with Google"
                                        onSuccess={handleRegister}
                                        onFailure={handleRegister}
                                        cookiePolicy={'single_host_origin'}
                                    />
                                </div>
                            </>
                        }
                    </Form>
                )}
            </Formik>
        </>
    )
}

export default MobileRegister
