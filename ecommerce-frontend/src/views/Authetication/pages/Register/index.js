import React, { useState } from 'react';
import { useMutation } from 'react-query';

import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import axiosInstances from '../../../../helper/axios';
import { errorResponse } from '../../../../helper/errorResponse';
import MobileRegister from './container/MobileRegister/MobileRegister';
import OTP from './container/OTP/OTP';
import UserRegister from './container/UserRegister';

const Register = () => {
    const navigate = useNavigate();

    const [loading, setLoading] = useState(false);
    const [phase, setPhase] = useState("mobile-register");
    const [otp, setOTP] = useState(false);
    const [mobile, setMobile] = useState('');

    const [data, setData] = useState({
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        confirmPassword: "",
        gender: "",
        dob: "",
        street: "",
        city: "",
        country: "",
        mobileNo: ""
    });

    const mutation = useMutation(async () => {
        setLoading(true);
        data.mobileNo = mobile
        return await axiosInstances.post('/user/register', data)
    }, {
        onSuccess: (res) => {
            toast.success("User Registered Successfully! Please login in to continue");
            navigate('/user/login')
        },
        onError: (res) => {
            errorResponse(res)
            setLoading(false)
        }
    });
    return (
        <div className='login mt-5'>
            <h4 className='text-muted mb-1'>
                Register for free !
            </h4>
            {
                phase === "mobile-register" && !otp &&
                <MobileRegister
                    loading={loading}
                    setLoading={setLoading}
                    setOTP={setOTP}
                    setMobile={setMobile}
                    type="register"
                />
            }
            {
                phase === "mobile-register" && otp &&
                <OTP
                    loading={loading}
                    setLoading={setLoading}
                    setPhase={setPhase}
                    mobile={mobile}
                />
            }
            {
                phase === "user-register" &&
                <UserRegister
                    loading={loading}
                    data={data}
                    setData={setData}
                    mutation={mutation}
                />
            }
        </div>
    )
}

export default Register
