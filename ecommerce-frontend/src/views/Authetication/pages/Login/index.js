import React, { useState } from 'react';
import { Formik } from 'formik';
import { Button, Col, Form, Row, Spinner } from 'react-bootstrap';
import TextInput from '../../../Landing/components/TextInput/TextInput';

import { useNavigate } from 'react-router-dom';

import * as Yup from 'yup';
import { mobileNoValidation, passwordValidation } from '../../../../helper/validation';
import { useMutation } from 'react-query';
import { errorResponse } from '../../../../helper/errorResponse';
import Cookies from 'js-cookie';
import axiosInstances from '../../../../helper/axios';
import { toast } from 'react-toastify';
import GoogleLogin from 'react-google-login';
import { useDispatch } from 'react-redux';
import { loginSuccess } from '../../../../redux/actions';

const loginValidation = Yup.object().shape({
    email: Yup.string().when("isEmail", {
        is: '1',
        then: Yup.string()
            .email("*Email is required")
            .required("*Email / Mobile Number is required"),
        otherwise: Yup.string()
            .required("*Email / Mobile Number cannot be empty")
            .matches(mobileNoValidation, "*Mobile Number is invalid"),
    }),
    password: Yup.string()
        .required("Password is required")
        .matches(passwordValidation, "Password must be minimum of 6 characters with number and letters")
})

const Login = () => {
    const navigate = useNavigate();
    const [loading, setLoading] = useState(false);
    const [visible, setVisible] = useState(false);

    const CLIENT_ID = process.env.REACT_APP_GOOGLE_CLIENT_ID;

    const dispatch = useDispatch();

    const mutation = useMutation(async (postData) => {
        return await axiosInstances.post('/user/login', postData)
    }, {
        onSuccess: (res) => {
            const result = {
                status: res.status + "-" + res.statusText,
                headers: res.headers,
                data: res.data,
            };
            dispatch(loginSuccess(result.data?.userDetail))
            toast.success(`Welcome ${result.data?.userDetail?.name}`);
            Cookies.set("token", result.data?.userDetail?.access_token);
            navigate('/')
            setLoading(false);
        },
        onError: (res) => {
            errorResponse(res)
            setLoading(false)
        }
    });

    const handleLogin = async (response) => {
        setLoading(true);
        try {
            const login = await axiosInstances.post('/user/google-login', {
                token: response?.tokenId
            });
            dispatch(loginSuccess(login.data?.userDetail))
            toast.success(`Welcome ${login.data?.userDetail?.name}`);
            Cookies.set("token", login.data?.userDetail?.access_token);
            navigate('/');
            setLoading(false);
        } catch (err) {
            errorResponse(err);
            setLoading(false);
        }
    }
    return (
        <div className='login mt-5'>
            <h4 className='text-muted mb-1'>
                Welcome to Freshnesecom !
            </h4>
            <h2>Login to Your Account</h2>
            <h5 className='mr-0 mt-4'>
                Not Registered Yet? <span onClick={() => navigate('/user/register')} className='text-secondary cursor-pointer'>
                    Create an account
                </span>
            </h5>
            <Formik
                initialValues={{ isEmail: "0", email: "", password: "" }}
                validationSchema={loginValidation}
                onSubmit={async (values) => {
                    setLoading(true);
                    let postData = {
                        loginId: values.email,
                        password: values.password,
                        fcmToken: ""
                    }

                    if (values.isEmail === "0") {
                        postData.fcmToken = "4274"
                    } else {
                        postData.fcmToken = "4275"
                    }
                    await mutation.mutate(postData);
                }}
            >
                {({
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                }
                ) => (
                    <Form onSubmit={handleSubmit}>
                        <Row className='mt-4'>
                            <Col xl="12">
                                <TextInput
                                    name="email"
                                    label="Phone Number or Email"
                                    type="text"
                                    value={values.email}
                                    handleChange={(event) => {
                                        handleChange("email")(event)
                                        if (Number(values.email)) {
                                            handleChange("isEmail")('0')
                                        } else {
                                            handleChange("isEmail")('1')
                                        }
                                    }}
                                    handleBlur={handleBlur}
                                    placeholder={"Please enter your email or phone number"}
                                    disabled={loading}
                                    touched={touched.email}
                                    errors={errors.email}
                                    autocomplete="off"
                                />
                            </Col>
                        </Row>
                        <Row className='mt-2' xl="12">
                            <TextInput
                                name="password"
                                label="Password"
                                type={visible ? 'text' : `password`}
                                setVisible={setVisible}
                                value={values.password}
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                                placeholder={"Minimum 6 characters with number and letters"}
                                disabled={loading}
                                touched={touched.password}
                                errors={errors.password}
                            />
                        </Row>
                        <Row>
                            <h5
                                onClick={() => navigate('/user/forget-password')}
                                className='text-secondary d-flex justify-content-end cursor-pointer mr-0'
                            >
                                Forget Password ?
                            </h5>
                        </Row>
                        <div className="mt-2">
                            <Button
                                type="submit"
                                size='sm'
                                className='w-100'
                                disabled={loading}
                            >
                                {
                                    loading ?
                                        <Spinner
                                            variant='light'
                                            animation='border'
                                            size='sm'
                                        /> :
                                        'Login'
                                }
                            </Button>
                        </div>
                        <div class="divider-line-x mt-2"><span>Or Login with</span></div>

                        <div className='mt-4'>
                            {/* <Button
                                type="button"
                                size='sm'
                                className='w-100 btn-secondary'
                                disabled={loading}
                            >
                                <Image
                                    width={25}
                                    className='mr-sm'
                                    src={google}
                                />
                                Sign in with Google
                            </Button> */}
                            <GoogleLogin
                                className='w-100 btn btn-secondary d-flex justify-content-center'
                                clientId={CLIENT_ID}
                                buttonText="Login with Google"
                                onSuccess={handleLogin}
                                onFailure={handleLogin}
                                cookiePolicy={'single_host_origin'}
                            />
                        </div>
                    </Form>
                )}
            </Formik>
        </div>
    )
}

export default Login
