import { Formik } from 'formik';
import React, { useState } from 'react'
import { Button, Col, Form, Row, Spinner } from 'react-bootstrap';
import * as Yup from 'yup'
import { passwordValidation } from '../../../../../helper/validation';
import TextInput from '../../../../Landing/components/TextInput/TextInput';


const newPasswordValidationSchema = Yup.object().shape({
    password: Yup.string()
        .required("Password is required")
        .matches(passwordValidation, "Password must be minimum of 6 characters with number and letters"),
    confirmPassword: Yup.string()
        .oneOf([Yup.ref('password'), null], '*Passwords doesnot match')
});

const NewPassword = ({ loading, mutation, setLoading }) => {
    const [passwordVisible, setPasswordVisible] = useState(false);
    return (
        <>
            <h2>Change your Password</h2>
            <Formik
                initialValues={{ password: "", confirmPassword: "" }}
                validationSchema={newPasswordValidationSchema}
                onSubmit={(values, { setSubmitting, resetForm }) => {
                    setLoading(true);
                    mutation.mutate(values.password)
                }}
            >
                {({
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                }
                ) => (
                    <Form onSubmit={handleSubmit}>
                        <Row>
                            <Col xl="12">
                                <TextInput
                                    name="password"
                                    label="Password"
                                    type={passwordVisible ? 'text' : `password`}
                                    setVisible={setPasswordVisible}
                                    value={values.password}
                                    handleChange={handleChange}
                                    handleBlur={handleBlur}
                                    placeholder={"Enter Password"}
                                    disabled={loading}
                                    touched={touched.password}
                                    errors={errors.password}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col xl="12">
                                <TextInput
                                    name="confirmPassword"
                                    label="Confirm Password"
                                    type={passwordVisible ? 'text' : `password`}
                                    setVisible={setPasswordVisible}
                                    value={values.confirmPassword}
                                    handleChange={handleChange}
                                    handleBlur={handleBlur}
                                    placeholder={"Enter Confirm Password"}
                                    disabled={loading}
                                    touched={touched.confirmPassword}
                                    errors={errors.confirmPassword}
                                />
                            </Col>
                        </Row>
                        <div className="mt-2">
                            <Button
                                type="submit"
                                size='sm'
                                className='w-100'
                                disabled={loading}
                            >
                                {
                                    loading ?
                                        <Spinner
                                            variant='light'
                                            animation='border'
                                            size='sm'
                                        /> :
                                        'Change Password'
                                }
                            </Button>
                        </div>
                    </Form>
                )}
            </Formik>
        </>
    )
}

export default NewPassword
