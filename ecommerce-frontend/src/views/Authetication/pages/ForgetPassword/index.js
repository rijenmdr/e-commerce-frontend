import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import MobileRegister from '../Register/container/MobileRegister/MobileRegister';
import OTP from '../Register/container/OTP/OTP';
import NewPassword from './container/NewPassword';
import { errorResponse } from '../../../../helper/errorResponse';
import { useMutation } from 'react-query';
import axiosInstances from '../../../../helper/axios';
import { toast } from 'react-toastify';

const ForgetPassword = () => {
    const navigate = useNavigate();
    const [phase, setPhase] = useState("authentication");
    const [loading, setLoading] = useState(false);
    const [otp, setOTP] = useState(false);
    const [mobile, setMobile] = useState('');

    const mutation = useMutation(async (password) => {
        const data = {
            mobileNo: mobile,
            password: password
        }
        return await axiosInstances.post('/user/forget-password', data)
    }, {
        onSuccess: (res) => {
            toast.success("Password Changed Successfully! Please login in to continue");
            navigate('/user/login')
        },
        onError: (res) => {
            errorResponse(res)
            setLoading(false)
        }
    });
    return (
        <div className='login mt-5'>
             {
                phase === "authentication" && !otp &&
                <MobileRegister
                    loading={loading}
                    setLoading={setLoading}
                    setOTP={setOTP}
                    setMobile={setMobile}
                    type="forget-password"
                />
            }
            {
                phase === "authentication" && otp &&
                <OTP
                    loading={loading}
                    setLoading={setLoading}
                    setPhase={setPhase}
                    mobile={mobile}
                />
            }
            {
                phase === "user-register" &&
                <NewPassword
                    setLoading={setLoading}
                    loading={loading}
                    mutation={mutation}
                />
            }
        </div>
    )
}

export default ForgetPassword
