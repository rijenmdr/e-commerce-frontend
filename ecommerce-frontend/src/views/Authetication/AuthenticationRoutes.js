import React, { Suspense } from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'
import Loading from '../Landing/components/Loading';

const Login = React.lazy(() =>
    import('./pages/Login')
);

const Register = React.lazy(() =>
    import('./pages/Register')
);

const ForgetPassword = React.lazy(() =>
    import('./pages/ForgetPassword')
);

const AuthenticationRoutes = (props) => {
    return (
        <Suspense fallback={<Loading />}>
            <Routes>
                <Route path={`/forget-password`} name="forget-password" element={<ForgetPassword />} />
                <Route path={`/register`} name="register" element={<Register />} />
                <Route path={`/login`} name="login" element={<Login />} />
                <Route path='/*' name="error" element={<Navigate to="login" />} />
            </Routes>
        </Suspense>
    )
}

export default AuthenticationRoutes
