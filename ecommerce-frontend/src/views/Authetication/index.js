import React from 'react'
import AuthenticationRoutes from './AuthenticationRoutes'
import HOC from './HOC';

import '../../assets/scss/component.scss';
import './Authentication.scss';


const Authentication = () => {
    return (
        <div className=''>
            <HOC>
                <AuthenticationRoutes />
            </HOC>
        </div>
    )
}

export default Authentication
